package io.evercam.capture;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.evercam.API;
import io.evercam.ApiKeyPair;
import io.evercam.EvercamException;
import io.evercam.User;
import io.evercam.UserDetail;
import io.evercam.capture.account.AccountUtils;
import io.evercam.capture.account.UserProfile;
import io.evercam.capture.authentication.EvercamAccount;
import io.evercam.capture.authentication.EvercamUser;
import io.evercam.capture.helper.Constants;
import io.evercam.capture.helper.DataCollector;
import io.evercam.capture.helper.PropertyReader;

public class SignUpActivity extends ActionBarActivity
{
    private final String TAG = "SignUpActivity";

    // Auto filled profiles
    private String filledFirstname = "";
    private String filledLastname = "";
    private String filledEmail = "";

    private EditText firstnameEdit;
    private EditText lastnameEdit;
    private EditText usernameEdit;
    private EditText emailEdit;
    private EditText passwordEdit;
    private EditText repasswordEdit;
    private View signUpFormView;
    private View signUpStatusView;
    private CreateUserTask createUserTask;
    private PropertyReader propertyReader;
    private View focusView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        EvercamCapture.sendScreenAnalytics(this, getString(R.string.screen_sign_up));

        setContentView(R.layout.activity_sign_up);

        readFromAccount();

        initialPage();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if(LoginActivity.isUserLogged(this))
        {
            setResult(Constants.RESULT_TRUE);
            finish();
        }
    }

    private void initialPage()
    {
        signUpFormView = findViewById(R.id.signup_form);
        signUpStatusView = findViewById(R.id.signup_status);
        firstnameEdit = (EditText) findViewById(R.id.forename_edit);
        lastnameEdit = (EditText) findViewById(R.id.lastname_edit);
        usernameEdit = (EditText) findViewById(R.id.username_edit);
        emailEdit = (EditText) findViewById(R.id.email_edit);
        passwordEdit = (EditText) findViewById(R.id.password_edit);
        repasswordEdit = (EditText) findViewById(R.id.repassword_edit);
        Button signupBtn = (Button) findViewById(R.id.sign_up_button);

        fillDefaultProfile();
        signupBtn.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UserDetail userDetail = checkDetails();
                if(userDetail != null)
                {
                    if(createUserTask != null)
                    {
                        createUserTask = null;
                    }
                    createUserTask = new CreateUserTask(userDetail);
                    createUserTask.execute();
                }
                else
                {
                    if(focusView != null)
                    {
                        focusView.requestFocus();
                    }
                }
            }
        });
    }

    private UserDetail checkDetails()
    {
        UserDetail user = new UserDetail();
        String firstname = firstnameEdit.getText().toString();
        String lastname = lastnameEdit.getText().toString();
        String username = usernameEdit.getText().toString();
        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        String repassword = repasswordEdit.getText().toString();

        firstnameEdit.setError(null);
        lastnameEdit.setError(null);
        usernameEdit.setError(null);
        emailEdit.setError(null);
        passwordEdit.setError(null);
        repasswordEdit.setError(null);

        if(TextUtils.isEmpty(firstname))
        {
            firstnameEdit.setError(getString(R.string.error_field_required));
            focusView = firstnameEdit;
            return null;
        }
        else
        {
            user.setFirstname(firstname);
        }

        if(TextUtils.isEmpty(lastname))
        {
            lastnameEdit.setError(getString(R.string.error_field_required));
            focusView = lastnameEdit;
            return null;
        }
        else
        {
            user.setLastname(lastname);
        }

        if(TextUtils.isEmpty(username))
        {
            usernameEdit.setError(getString(R.string.error_field_required));
            focusView = usernameEdit;
            return null;
        }
        else if(username.length() < 3)
        {
            usernameEdit.setError(getString(R.string.username_too_short));
            focusView = usernameEdit;
            return null;
        }
        else if(username.contains(" "))
        {
            usernameEdit.setError(getString(R.string.error_invalid_username));
            focusView = usernameEdit;
            return null;
        }
        else
        {
            user.setUsername(username);
        }

        if(TextUtils.isEmpty(email))
        {
            emailEdit.setError(getString(R.string.error_field_required));
            focusView = emailEdit;
            return null;
        }
        else if(!email.contains("@"))
        {
            makeShortToast(R.string.invalidEmail);
            focusView = emailEdit;
            return null;
        }
        else
        {
            user.setEmail(email);
        }

        if(TextUtils.isEmpty(password))
        {
            passwordEdit.setError(getString(R.string.error_field_required));
            focusView = passwordEdit;
            return null;
        }

        if(TextUtils.isEmpty(repassword))
        {
            repasswordEdit.setError(getString(R.string.error_field_required));
            focusView = repasswordEdit;
            return null;
        }
        else if(!password.equals(repassword))
        {
            makeShortToast(R.string.passwordNotMatch);
            return null;
        }
        else
        {
            user.setPassword(password);
        }

        //Append user country if available, but the country field will not be showing in sign up
        // form
        String countryCode = DataCollector.getCountryCode(this);
        if(countryCode != null && !countryCode.isEmpty())
        {
            user.setCountrycode(countryCode);
        }

        return user;
    }

    private void makeShortToast(int message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void makeShortToast(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public class CreateUserTask extends AsyncTask<Void, Void, String>
    {
        UserDetail userDetail;

        public CreateUserTask(UserDetail userDetail)
        {
            this.userDetail = userDetail;
        }

        @Override
        protected void onPostExecute(String message)
        {
            if(message == null)
            {
                showConfirmSignUp();
            }
            else
            {
                showProgress(false);
                makeShortToast(message);
            }
        }

        @Override
        protected void onPreExecute()
        {
            //Hide soft keyboard
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            if(inputMethodManager != null && SignUpActivity.this.getCurrentFocus() != null)
            {
                inputMethodManager.hideSoftInputFromWindow(SignUpActivity.this.getCurrentFocus()
                        .getWindowToken(), 0);
            }

            showProgress(true);
        }

        @Override
        protected String doInBackground(Void... args)
        {
            try
            {
                User.create(userDetail);
                ApiKeyPair userKeyPair = API.requestUserKeyPairFromEvercam(userDetail.getUsername
                        (), userDetail.getPassword());
                String userApiKey = userKeyPair.getApiKey();
                String userApiId = userKeyPair.getApiId();
                API.setUserKeyPair(userApiKey, userApiId);
                User user = new User(userDetail.getUsername());
                EvercamUser newUser = new EvercamUser(user);
                newUser.setApiKeyPair(userApiKey, userApiId);

                //The new added user is always configured to be default
                new EvercamAccount(SignUpActivity.this).add(newUser);
                return null;
            }
            catch(EvercamException e)
            {
                return e.getMessage();
            }
        }
    }

    private void readFromAccount()
    {
        try
        {
            UserProfile profile = AccountUtils.getUserProfile(this);
            if(profile.primaryEmail() != null)
            {
                filledEmail = profile.primaryEmail();
            }
            else if(profile.possibleEmails().size() > 0)
            {
                filledEmail = profile.possibleEmails().get(0);
            }

            if(profile.possibleNames().size() > 0)
            {
                String name = profile.possibleNames().get(0);
                String[] nameArray = name.split("\\s+");
                if(nameArray.length >= 2)
                {
                    filledFirstname = nameArray[0];
                    filledLastname = nameArray[1];
                }
            }
        }
        catch(Exception e)
        {
            // If exceptions happen here, will not influence app functionality.
            // Just catch it to avoid crashing.
            Log.e(TAG, e.toString());
        }
    }

    private void fillDefaultProfile()
    {
        firstnameEdit.setText(filledFirstname);
        lastnameEdit.setText(filledLastname);
        emailEdit.setText(filledEmail);
    }

    private void showProgress(boolean show)
    {
        signUpStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
        signUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showConfirmSignUp()
    {
        makeShortToast(R.string.confirmSignUp);
        showProgress(false);
        setResult(Constants.RESULT_TRUE);
        finish();
    }
}
