package io.evercam.capture;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.Session;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.evercam.EvercamException;
import io.evercam.capture.authentication.EvercamAccount;
import io.evercam.capture.authentication.EvercamData;
import io.evercam.capture.helper.CameraHelper;
import io.evercam.capture.helper.CustomedDialog;
import io.evercam.capture.helper.DataCollector;
import io.evercam.capture.helper.NetInfo;
import io.evercam.capture.helper.PrefsManager;
import io.evercam.capture.helper.TakePictureHelper;
import io.evercam.capture.server.ServerThread;
import io.evercam.capture.tasks.AddToEvercamTask;
import io.evercam.capture.tasks.CameraCheckTask;
import io.evercam.capture.tasks.CheckInternetTask;
import io.evercam.capture.tasks.DeleteCameraTask;
import io.evercam.capture.tasks.PatchEndpointTask;
import io.evercam.capture.tasks.PatchOnlineTask;
import io.evercam.capture.tasks.PatchTask;
import io.evercam.capture.tasks.SshForwardingTask;

public class MainActivity extends ParentActionBarActivity
{
    public final static int FRONT_CAMERA = 0;
    public final static int BACK_CAMERA = 1;
    public final static int BOTH_CAMERA = 2;

    public final String TAG = "capture-MainActivity";

    public Session sshSession;
    public SurfaceView surfaceView;
    public ProgressBar frontProgress;
    public ProgressBar backProgress;
    public io.evercam.Camera frontCamera;
    public io.evercam.Camera backCamera;
    public boolean frontAdded;
    public boolean backAdded;
    public boolean cameraInUse = false;
    public int selectedCamera = -1;
    public int addTimeCount = 0;

    private ServerThread serverThread;
    private Thread thread;
    private ProgressDialog progressDialog;
    private TextView progressTextView;
    private ScrollView progressScrollView;
    private SharedPreferences sharedPrefs;
    private PowerManager.WakeLock wakeLock;
    private TakePictureHelper helper;
    private ImageView frontImage;
    private ImageView backImage;
    private boolean refreshing = false;
    private String usernameOnStop = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        PrefsManager.setPort(sharedPrefs, PrefsManager.DEFAULT_PORT);

        keepScreenOn();

        new CheckInternetTaskMain(MainActivity.this, CheckInternetTaskMain.TAG_INITIAL).execute();
    }

    @Override
    public void onBackPressed()
    {
        showConfirmQuitDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem logoutItem = menu.findItem(R.id.action_logout);
        String logoutItemText = getString(R.string.sign_out);
        if(EvercamData.defaultUser != null)
        {
            logoutItemText += " - " + EvercamData.defaultUser.getUsername();
        }
        logoutItem.setTitle(logoutItemText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.action_logout)
        {
            showConfirmLogout();
        }
        else if(item.getItemId() == R.id.action_refresh)
        {
            EvercamCapture.sendEventAnalytics(this, R.string.category_menu, R.string
                    .action_refresh, R.string.label_refresh);
            new CheckInternetTaskMain(MainActivity.this, CheckInternetTaskMain.TAG_REFRESH)
                    .execute();
        }
        else if(item.getItemId() == R.id.action_dashboard)
        {
            patchOffline();
            CustomedDialog.getLeavePageWarningDialog(this).show();
        }
        else if(item.getItemId() == R.id.action_switch_camera)
        {
            patchOffline();

            EvercamCapture.sendEventAnalytics(this, R.string.category_menu, R.string
                    .action_switch_camera, R.string.label_switch_camera);

            stopServerThread();
            if(helper != null)
            {
                helper.removePreviewCallback();
            }
            serverThread = null;
            thread = null;
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.action_resolution)
        {
            showResolutionDialog();
        }
        else if(item.getItemId() == R.id.action_about)
        {
            String appVersion = getString(R.string.version) + DataCollector.getAppVersion(this);
            Toast.makeText(this, appVersion, Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    public void runCameraCheck()
    {
        CameraCheckTask cameraCheckTask = new CameraCheckTask(MainActivity.this);
        cameraCheckTask.execute();
    }

    private void initialPage()
    {
        surfaceView = (SurfaceView) findViewById(R.id.surface);

        if(selectedCamera == BOTH_CAMERA)
        {
            backImage = (ImageView) findViewById(R.id.backImage);
            backProgress = ((ProgressBar) findViewById(R.id.progressBarBack));
            backImage.setOnClickListener(new OnClickListener()
            {

                @Override
                public void onClick(View arg0)
                {
                    if(backCamera != null)
                    {
                        showEditDialog(Camera.CameraInfo.CAMERA_FACING_BACK);
                    }
                }
            });
        }
        if(selectedCamera == BOTH_CAMERA)
        {
            frontImage = (ImageView) findViewById(R.id.frontImage);
            frontProgress = ((ProgressBar) findViewById(R.id.progressBarFront));

            frontImage.setOnClickListener(new OnClickListener()
            {

                @Override
                public void onClick(View arg0)
                {
                    if(frontCamera != null)
                    {
                        showEditDialog(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    }
                }
            });
        }

        progressTextView = (TextView) findViewById(R.id.terminalTextView);
        progressScrollView = (ScrollView) findViewById(R.id.scrollView);

        helper = new TakePictureHelper(MainActivity.this);

        if(CameraHelper.hasFrontCamera(this))
        {
            // FIXME: check device has front camera or not
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        if(EvercamData.defaultUser != null)
        {
            usernameOnStop = EvercamData.defaultUser.getUsername();
        }

        patchOffline();

        if(wakeLock.isHeld())
        {
            wakeLock.release();
        }

        releaseCameraResources();
        ;

        disconnectSshSession();
        stopServerThread();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if(LoginActivity.isUserLogged(this))
        {
            if(!wakeLock.isHeld())
            {
                wakeLock.acquire();
            }

            String restartedUsername = EvercamData.defaultUser.getUsername();

            //Reload camera list if default user has been changed
            if(!usernameOnStop.isEmpty() && !usernameOnStop.equals(restartedUsername))
            {
                new CheckInternetTaskMain(MainActivity.this, CheckInternetTaskMain.TAG_INITIAL)
                        .execute();
            }
            else
            {
                new CheckInternetTaskMain(MainActivity.this, CheckInternetTaskMain.TAG_RESTART)
                        .execute();
            }
            usernameOnStop = "";
        }
        else
        {
            Intent intentSlide = new Intent(this, SlideActivity.class);
            startActivity(intentSlide);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        patchOffline();

        if(helper != null)
        {
            if(helper.camera != null && helper.surfaceHolder != null)
            {
                helper.camera.release();
                helper.removeHolderCallback();
            }
        }
        stopServerThread();
    }

    public void updateProgress(final ProgressBar progressBar, final boolean visible)
    {
        this.runOnUiThread(new Runnable()
        {

            @Override
            public void run()
            {
                if(progressBar != null)
                {
                    if(visible)
                    {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    public void showShortToast(String text)
    {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void startServer()
    {
        refreshing = true;
        int localPort = PrefsManager.getPort(sharedPrefs);
        serverThread = new ServerThread(localPort, MainActivity.this, helper);
        thread = new Thread(serverThread);
        thread.start();

        new SshForwardingTask(MainActivity.this, localPort).execute();
    }

    public void showPhotoTaken(final Bitmap bitmap, int cameraID)
    {
        final Runnable removeFrontBorder = new Runnable()
        {
            @Override
            public void run()
            {
                frontImage.setBackgroundResource(R.drawable.imgview_border_null);
            }
        };

        final Runnable removeBackBorder = new Runnable()
        {
            @Override
            public void run()
            {
                backImage.setBackgroundResource(R.drawable.imgview_border_null);
            }
        };

        if(cameraID == Camera.CameraInfo.CAMERA_FACING_FRONT && selectedCamera == BOTH_CAMERA)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    frontImage.setImageBitmap(bitmap);
                    frontImage.setBackgroundResource(R.drawable.imgview_border);
                    frontImage.postDelayed(removeFrontBorder, 1500);
                }
            });
        }
        else if(cameraID == Camera.CameraInfo.CAMERA_FACING_BACK && selectedCamera == BOTH_CAMERA)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    backImage.setImageBitmap(bitmap);
                    backImage.setBackgroundResource(R.drawable.imgview_border);
                    backImage.postDelayed(removeBackBorder, 1500);
                }
            });
        }
    }

    public void showProgress(final String msg)
    {
        this.runOnUiThread(new Runnable()
        {

            @Override
            public void run()
            {
                progressDialog = ProgressDialog.show(MainActivity.this, "", msg, true);
            }
        });
    }

    public void dismissProgress()
    {
        this.runOnUiThread(new Runnable()
        {

            @Override
            public void run()
            {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void onCameraCheckDone()
    {
        String internalHost = PrefsManager.getInternalHost(sharedPrefs);
        int internalPort = PrefsManager.getInternalPort(sharedPrefs);
        String externalHost = PrefsManager.getExternalHost(sharedPrefs);
        int externalPort = PrefsManager.getExternalPort(sharedPrefs);
        saveCameraCredentials();
        try
        {
            if(selectedCamera == FRONT_CAMERA)
            {
                if(frontAdded)
                {
                    showSingleCameraDetails(frontCamera);

                    refreshing = false;

                    patchLocalHost(frontCamera, Camera.CameraInfo.CAMERA_FACING_FRONT,
                            internalHost, internalPort);

                    // Update camera to 'online', and update location.
                    patchOnlineAndLocation(frontCamera.getId(), true);
                }
                else
                {
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, FRONT_CAMERA).execute();
                }
            }
            else if(selectedCamera == BACK_CAMERA)
            {
                if(backAdded)
                {
                    showSingleCameraDetails(backCamera);

                    refreshing = false;

                    patchLocalHost(backCamera, Camera.CameraInfo.CAMERA_FACING_BACK,
                            internalHost, internalPort);

                    patchOnlineAndLocation(backCamera.getId(), true);
                }
                else
                {
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, BACK_CAMERA).execute();
                }
            }
            else if(selectedCamera == BOTH_CAMERA)
            {
                if(frontAdded && backAdded)
                {
                    refreshing = false;
                    appendTextProgress(getString(R.string.log_added_to_evercam));

					/* Show camera ID */
                    TextView frontIdView = (TextView) findViewById(R.id.frontId);
                    TextView backIdView = (TextView) findViewById(R.id.backId);
                    try
                    {
                        frontIdView.setText(getString(R.string.evercam_id) + " " + frontCamera
                                .getId());
                        backIdView.setText(getString(R.string.evercam_id) + " " + backCamera
                                .getId());
                        frontIdView.setVisibility(View.VISIBLE);
                        backIdView.setVisibility(View.VISIBLE);
                    }
                    catch(EvercamException e)
                    {
                        Log.e(TAG, e.getMessage());
                    }

                    patchLocalHost(backCamera, Camera.CameraInfo.CAMERA_FACING_BACK,
                            internalHost, internalPort);
                    patchLocalHost(frontCamera, Camera.CameraInfo.CAMERA_FACING_FRONT,
                            internalHost, internalPort);
                }
                else if(frontAdded && !backAdded)
                {
                    patchLocalHost(frontCamera, Camera.CameraInfo.CAMERA_FACING_FRONT,
                            internalHost, internalPort);
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, BACK_CAMERA).execute();

                    patchOnlineAndLocation(frontCamera.getId(), true);
                }
                else if(!frontAdded && backAdded)
                {
                    patchLocalHost(backCamera, Camera.CameraInfo.CAMERA_FACING_BACK,
                            internalHost, internalPort);
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, FRONT_CAMERA).execute();

                    patchOnlineAndLocation(backCamera.getId(), true);
                }
                else
                {
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, FRONT_CAMERA).execute();
                    new AddToEvercamTask(MainActivity.this, internalHost, internalPort,
                            externalHost, externalPort, BACK_CAMERA).execute();
                }
            }
        }
        catch(EvercamException e)
        {
            Log.e(TAG, "onCameraCheckDone:" + e.getMessage());
        }
    }

    private void showEditDialog(final int cameraId)
    {
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        final View editView = mInflater.inflate(R.layout.edit_dialog, null);
        final AlertDialog.Builder editBuilder = new AlertDialog.Builder(this);
        editBuilder.setView(editView);
        EditText idEdit = (EditText) editView.findViewById(R.id.id_value);
        final EditText nameEdit = (EditText) editView.findViewById(R.id.name_value);
        EditText snapshotEdit = (EditText) editView.findViewById(R.id.snapshot_value);
        EditText usernameEdit = (EditText) editView.findViewById(R.id.username_value);
        EditText passwordEdit = (EditText) editView.findViewById(R.id.password_value);
        EditText macEdit = (EditText) editView.findViewById(R.id.mac_value);
        EditText vendorEdit = (EditText) editView.findViewById(R.id.vendor_value);
        EditText modelEdit = (EditText) editView.findViewById(R.id.model_value);
        TextView endpointText = (TextView) editView.findViewById(R.id.endpoints_value);
        final RadioButton publicRadio = (RadioButton) editView.findViewById(R.id.publicRadio);
        RadioButton privateRadio = (RadioButton) editView.findViewById(R.id.privateRadio);

        editBuilder.setNegativeButton(R.string.cancel, null);
        editBuilder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String name = nameEdit.getText().toString();
                boolean isPublic;
                if(publicRadio.isChecked())
                {
                    isPublic = true;
                }
                else
                {
                    isPublic = false;
                }
                if(!name.equals(""))
                {
                    new PatchTask(cameraId, name, isPublic, MainActivity.this).execute();
                }
                else
                {
                    showShortToast(getString(R.string.empty_camera_name));
                }
            }
        });

        if(cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            editBuilder.setTitle(R.string.edit_front_title);
            try
            {
                idEdit.setText(frontCamera.getId());
                nameEdit.setText(frontCamera.getName());
                snapshotEdit.setText(getSnapshotPath(frontCamera.getInternalJpgUrl()));
                usernameEdit.setText(frontCamera.getUsername());
                passwordEdit.setText(frontCamera.getPassword());
                macEdit.setText(frontCamera.getMacAddress());
                vendorEdit.setText(frontCamera.getVendorName());
                modelEdit.setText(frontCamera.getModelName());
                if(!frontCamera.isPublic())
                {
                    privateRadio.setChecked(true);
                }
                endpointText.setText(getEndpointsText(frontCamera));
            }
            catch(EvercamException e)
            {
                Log.e(TAG, "read camera details: " + e.getMessage());
            }
            catch(Exception e)
            {
                Mint.logException(e);
            }
        }
        else if(cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
        {
            editBuilder.setTitle(R.string.edit_back_title);
            try
            {
                idEdit.setText(backCamera.getId());
                nameEdit.setText(backCamera.getName());
                snapshotEdit.setText(getSnapshotPath(backCamera.getInternalJpgUrl()));
                usernameEdit.setText(backCamera.getUsername());
                passwordEdit.setText(backCamera.getPassword());
                macEdit.setText(backCamera.getMacAddress());
                vendorEdit.setText(backCamera.getVendorName());
                modelEdit.setText(backCamera.getModelName());
                if(!backCamera.isPublic())
                {
                    privateRadio.setChecked(true);
                }
                endpointText.setText(getEndpointsText(backCamera));
            }
            catch(EvercamException e)
            {
                Log.e(TAG, "read camera details: " + e.getMessage());
            }
        }
        editBuilder.setCancelable(false);
        editBuilder.show();
    }

    private void showConfirmLogout()
    {
        AlertDialog confirmLogoutDialog = CustomedDialog.getBasicConfirmDialog(this, R.string
                .confirmLogOutMsg, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        EvercamCapture.sendEventAnalytics(MainActivity.this, R.string
                                .category_menu, R.string.action_logout, R.string
                                .label_confirm_logout);
                        signOutUser();
                    }
                });
        confirmLogoutDialog.show();
    }

    private void showConfirmQuitDialog()
    {
        AlertDialog confirmQuitDialog = CustomedDialog.getBasicConfirmDialog(this, R.string
                .confirmQuitMsg, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                finish();
            }
        });
        confirmQuitDialog.show();
    }

    private void signOutUser()
    {
        String emailToSignOut = EvercamData.defaultUser.getEmail();
        new EvercamAccount(this).remove(emailToSignOut, null);
        startActivity(new Intent(MainActivity.this, SlideActivity.class));
        finish();
    }

    private void resetForRefresh()
    {
        frontCamera = null;
        backCamera = null;
        frontAdded = false;
        backAdded = false;
    }

    private void saveCameraCredentials()
    {
        try
        {
            PrefsManager.saveCameraCredentials(sharedPrefs, frontCamera, backCamera);
        }
        catch(EvercamException e)
        {
            e.printStackTrace();
        }
    }

    private String getCurrentTime()
    {
        final String TIME_FORMAT = "hh:mm:ss";
        Date date = new Date();
        CharSequence charSequence = DateFormat.format(TIME_FORMAT, date.getTime());
        return String.valueOf(charSequence);
    }

    public void appendCameraDetails(io.evercam.Camera camera)
    {
        if(camera != null)
        {
            try
            {
                appendTextProgress("'" + camera.getName() + "'" + getString(R.string
                        .camera_is_connected));
                appendTextProgress(getString(R.string.url) + camera.getInternalJpgUrl());
                appendTextProgress(getString(R.string.url) + camera.getExternalJpgUrl());
                appendTextProgress(getString(R.string.username_) + camera.getUsername());
                appendTextProgress(getString(R.string.password_) + camera.getPassword());
            }
            catch(EvercamException e)
            {
                Log.e(TAG, "appendCameraDetails" + e.toString());
            }
        }
        else
        {
            appendTextProgress(getString(R.string.camera_is_null));
        }
    }

    public void appendTextProgress(final String progressText)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                appendColoredText(progressTextView, getCurrentTime() + ": ", Color.parseColor
                        ("#808080"));
                if(!(progressTextView == null && progressScrollView == null))
                {
                    progressTextView.append(progressText + "\n");
                    progressScrollView.fullScroll(View.FOCUS_DOWN);
                }
            }
        });
    }

    private void appendColoredText(TextView textView, String text, int color)
    {
        if(textView != null)
        {
            int start = textView.getText().length();
            textView.append(text);
            int end = textView.getText().length();

            Spannable spannableText = (Spannable) textView.getText();
            spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
        }
    }

    private void showChooseCameraDialog()
    {
        try
        {
            new DeleteCameraTask(MainActivity.this).execute();
            resetForRefresh();

            AlertDialog.Builder chooseCameraBuilder = new AlertDialog.Builder(MainActivity.this);
            chooseCameraBuilder.setTitle(R.string.please_select_camera);
            chooseCameraBuilder.setCancelable(false);
            chooseCameraBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    selectedCamera = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    if(selectedCamera == FRONT_CAMERA)
                    {
                        setContentView(R.layout.activity_main_front);
                        EvercamCapture.sendScreenAnalytics(MainActivity.this, getString(R.string
                                .screen_front_camera));
                    }
                    else if(selectedCamera == BACK_CAMERA)
                    {
                        setContentView(R.layout.activity_main_back);
                        EvercamCapture.sendScreenAnalytics(MainActivity.this, getString(R.string
                                .screen_back_camera));
                    }
                    else if(selectedCamera == BOTH_CAMERA)
                    {
                        setContentView(R.layout.activity_main);
                        EvercamCapture.sendScreenAnalytics(MainActivity.this, getString(R.string
                                .screen_both_camera));
                    }

                    PrefsManager.saveSelectedCamera(sharedPrefs, selectedCamera);

                    initialPage();
                    appendTextProgress(getString(R.string.log_account_name) + "'" +
                            EvercamData.defaultUser.getUsername() + "'");
                    startServer();
                    return;
                }
            });
            chooseCameraBuilder.setSingleChoiceItems(R.array.camera_array, getSelectedCamera(),
                    null);
            chooseCameraBuilder.show();
        }
        catch(Exception e)
        {
            Mint.logException(e);
        }
    }

    @SuppressLint("NewApi")
    private void showSingleCameraDetails(io.evercam.Camera camera) throws EvercamException
    {
        try
        {
            TextView cameraNameText;
            if(selectedCamera == FRONT_CAMERA)
            {
                cameraNameText = (TextView) findViewById(R.id.front_camera_text);
                cameraNameText.setText(camera.getName());
                Button frontEditButton = (Button) findViewById(R.id.button_editfrontname);
                frontEditButton.setVisibility(View.VISIBLE);
                frontEditButton.setOnClickListener(new OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CustomedDialog.getEditNameDialog(MainActivity.this, Camera.CameraInfo
                                .CAMERA_FACING_FRONT).show();
                    }
                });
            }
            else if(selectedCamera == BACK_CAMERA)
            {
                cameraNameText = (TextView) findViewById(R.id.back_camera_text);
                cameraNameText.setText(camera.getName());
                Button backEditButton = (Button) findViewById(R.id.button_editbackname);
                backEditButton.setVisibility(View.VISIBLE);
                backEditButton.setOnClickListener(new OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CustomedDialog.getEditNameDialog(MainActivity.this, Camera.CameraInfo
                                .CAMERA_FACING_BACK).show();
                    }
                });
            }
            LinearLayout evercamIdLayout = (LinearLayout) findViewById(R.id.camera_id_layout);
            LinearLayout cameraOwnerLayout = (LinearLayout) findViewById(R.id.camera_owner_layout);
            TextView cameraIdValue = (TextView) findViewById(R.id.cameraIdValue);
            TextView cameraOwnerValue = (TextView) findViewById(R.id.cameraOwnerValue);
            ProgressBar singleProgress = ((ProgressBar) findViewById(R.id.progressBarSingle));
            View line = findViewById(R.id.grayLine);

            if(camera != null)
            {
                // For new API use Switch, otherwise use CheckBox instead.
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                {
                    LinearLayout isPublicLayout = (LinearLayout) findViewById(R.id.switch_layout);
                    final Switch isPublicSwitch = (Switch) findViewById(R.id.is_public_switch);
                    isPublicLayout.setVisibility(View.VISIBLE);
                    isPublicSwitch.setChecked(camera.isPublic() && camera.isDiscoverable());
                    isPublicSwitch.setOnClickListener(new OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            patchCameraIsPublicOnly(isPublicSwitch.isChecked());
                        }
                    });
                }
                else
                {
                    LinearLayout isPublicLayout = (LinearLayout) findViewById(R.id
                            .is_public_layout);
                    final CheckBox isPublicCheckbox = (CheckBox) findViewById(R.id
                            .is_public_checkbox);
                    isPublicLayout.setVisibility(View.VISIBLE);
                    isPublicCheckbox.setChecked(camera.isPublic() && camera.isDiscoverable());
                    isPublicCheckbox.setOnClickListener(new OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            patchCameraIsPublicOnly(isPublicCheckbox.isChecked());
                        }
                    });
                }

                evercamIdLayout.setVisibility(View.VISIBLE);
                cameraOwnerLayout.setVisibility(View.VISIBLE);
                cameraIdValue.setText(camera.getId());
                cameraOwnerValue.setText(camera.getOwner());

                singleProgress.setVisibility(View.INVISIBLE);
                line.setVisibility(View.VISIBLE);
            }
        }
        catch(Exception e)
        {
            Log.e(TAG, e.toString());
            Mint.logException(e);
        }
    }

    public void updateAfterPatch()
    {
        CheckBox isPublicCheckbox = (CheckBox) findViewById(R.id.is_public_checkbox);
        try
        {
            if(selectedCamera == FRONT_CAMERA)
            {
                TextView nameText = (TextView) findViewById(R.id.front_camera_text);
                nameText.setText(frontCamera.getName());
                isPublicCheckbox.setChecked(frontCamera.isPublic());
            }
            else if(selectedCamera == BACK_CAMERA)
            {
                TextView nameText = (TextView) findViewById(R.id.back_camera_text);
                nameText.setText(backCamera.getName());
                isPublicCheckbox.setChecked(backCamera.isPublic());
            }
        }
        catch(EvercamException e)
        {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void updateAfterPatchAPI14()
    {
        Switch isPublicSwitch = (Switch) findViewById(R.id.is_public_switch);
        try
        {
            if(selectedCamera == FRONT_CAMERA)
            {
                TextView nameText = (TextView) findViewById(R.id.front_camera_text);
                nameText.setText(frontCamera.getName());
                isPublicSwitch.setChecked(frontCamera.isPublic());
            }
            else if(selectedCamera == BACK_CAMERA)
            {
                TextView nameText = (TextView) findViewById(R.id.back_camera_text);
                nameText.setText(backCamera.getName());
                isPublicSwitch.setChecked(backCamera.isPublic());
            }
        }
        catch(EvercamException e)
        {
            e.printStackTrace();
        }
    }

    private void stopServerThread()
    {
        if(serverThread != null)
        {
            serverThread.stop();
        }
    }

    private void disconnectSshSession()
    {
        if(sshSession != null)
        {
            sshSession.disconnect();
        }
    }

    private void keepScreenOn()
    {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "Evercam_stayAwake");
        wakeLock.acquire();
    }

    private String getEndpointsText(io.evercam.Camera camera)
    {
        String endpointStr = "";
        ArrayList<String> endpointsArray = camera.getEndpoints();

        for(String endpoint : endpointsArray)
        {
            endpointStr += endpoint + "\n";
        }
        return endpointStr;
    }

    private void patchCameraIsPublicOnly(boolean isPublic)
    {
        if(isPublic)
        {
            EvercamCapture.sendEventAnalytics(this, R.string.category_edit, R.string
                    .action_edit_public, getString(R.string.label_edit_public) + getString(R
                    .string.Public));
        }
        else
        {
            EvercamCapture.sendEventAnalytics(this, R.string.category_edit, R.string
                    .action_edit_public, getString(R.string.label_edit_public) + getString(R
                    .string.Private));
        }
        try
        {
            if(selectedCamera == FRONT_CAMERA && frontCamera != null)
            {
                new PatchTask(Camera.CameraInfo.CAMERA_FACING_FRONT, frontCamera.getName(),
                        isPublic, MainActivity.this).execute();
            }
            else if(selectedCamera == BACK_CAMERA && backCamera != null)
            {
                new PatchTask(Camera.CameraInfo.CAMERA_FACING_BACK, backCamera.getName(),
                        isPublic, MainActivity.this).execute();
            }
        }
        catch(EvercamException e)
        {
            Log.e(TAG, e.getMessage());
        }
        catch(Exception e)
        {
            Log.e(TAG, e.toString());
            Mint.logException(e);
        }
    }

    private boolean localHostMatches(io.evercam.Camera camera)
    {
        try
        {
            if(camera.getInternalHost().equals(NetInfo.getLocalIpAddress()))
            {
                return true;
            }
        }
        catch(EvercamException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private void patchLocalHost(io.evercam.Camera camera, int cameraId, String internalHost, int
            internalPort)
    {
        if(!localHostMatches(camera))
        {
            new PatchEndpointTask(cameraId, MainActivity.this, internalHost, internalPort)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            if(selectedCamera != BOTH_CAMERA)
            {
                appendCameraDetails(camera);
            }
        }
    }

    private int getSelectedCamera()
    {
        int selectedCamera = PrefsManager.getSelectedCamera(sharedPrefs);
        if(selectedCamera >= 0)
        {
            return selectedCamera;
        }
        return BACK_CAMERA;
    }

    public class CheckInternetTaskMain extends CheckInternetTask
    {
        public final static int TAG_INITIAL = 0;
        public final static int TAG_REFRESH = 1;
        public final static int TAG_RESTART = 2;

        private int flag;

        public CheckInternetTaskMain(Context context, int flag)
        {
            super(context);
            this.flag = flag;
        }

        @Override
        protected void onPostExecute(Boolean hasNetwork)
        {
            if(hasNetwork)
            {
                if(flag == TAG_INITIAL)
                {
                    showChooseCameraDialog();
                }
                else if(flag == TAG_REFRESH)
                {
                    if(!refreshing)
                    {
                        disconnectSshSession();
                        stopServerThread();
                        resetForRefresh();
                        startServer();
                    }
                }
                else if(flag == TAG_RESTART)
                {
                    if(selectedCamera < 0)
                    {
                        showChooseCameraDialog();
                    }
                    else
                    {
                        disconnectSshSession();

                        restartCamera();

                        resetForRefresh();
                        startServer();
                    }
                }
            }
            else
            {
                try
                {
                    CustomedDialog.getNoInternetDialog(MainActivity.this).show();
                }
                catch(Exception e)
                {
                    // If activity no longer exists, do nothing.
                }
            }
        }
    }

    private String getSnapshotPath(String fullUrl)
    {
        return fullUrl.substring(fullUrl.indexOf("8080/") + 4, fullUrl.length());
    }

    private void patchOnlineAndLocation(String cameraId, Boolean isOnline)
    {
        new PatchOnlineTask(cameraId, isOnline, this).executeOnExecutor(AsyncTask
                .THREAD_POOL_EXECUTOR);
    }

    private void patchOffline()
    {
        try
        {
            if(selectedCamera == FRONT_CAMERA && frontCamera != null)
            {
                patchOnlineAndLocation(frontCamera.getId(), false);
            }
            else if(selectedCamera == BACK_CAMERA && backCamera != null)
            {
                patchOnlineAndLocation(backCamera.getId(), false);
            }
            else if(selectedCamera == BOTH_CAMERA && frontCamera != null && backCamera != null)
            {
                patchOnlineAndLocation(frontCamera.getId(), false);
                patchOnlineAndLocation(backCamera.getId(), false);
            }
        }
        catch(EvercamException e)
        {
            Log.e(TAG, "Error patch camera offline" + e.toString());
        }
    }

    private void showResolutionDialog()
    {
        final int oldSelectedPosition = getSelectedResolutionPosition();

        AlertDialog.Builder resolutionDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        resolutionDialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                int newSelectedPosition = ((AlertDialog) dialog).getListView()
                        .getCheckedItemPosition();
                if(newSelectedPosition != oldSelectedPosition)
                {
                    final String[] resolutions = getResources().getStringArray(R.array
                            .resolution_array);
                    PrefsManager.setImageResolution(resolutions[newSelectedPosition],
                            MainActivity.this);

                    if(helper != null && helper.camera != null)
                    {
                        helper.camera.release();
                        helper.startCameraPreview();
                    }
                }
            }
        });
        resolutionDialogBuilder.setSingleChoiceItems(R.array.resolution_array,
                oldSelectedPosition, null);
        resolutionDialogBuilder.show();
    }

    private int getSelectedResolutionPosition()
    {
        String selectedResolution = PrefsManager.getImageResolution(this);
        final String[] resolutions = getResources().getStringArray(R.array.resolution_array);
        List<String> list = Arrays.asList(resolutions);
        return list.indexOf(selectedResolution);
    }

    private void releaseCameraResources()
    {
        if(helper != null && helper.surfaceHolder != null)
        {
            helper.removeHolderCallback();

            if(helper.camera != null)
            {
                helper.camera.release();
            }
        }
        helper = null;
    }

    private void restartCamera()
    {
        if(helper != null)
        {
            if(helper.surfaceHolder != null)
            {
                helper.surfaceHolder.addCallback(helper);
            }
        }
        else
        {
            helper = new TakePictureHelper(MainActivity.this);
            helper.startCameraPreview();
        }
    }
}
