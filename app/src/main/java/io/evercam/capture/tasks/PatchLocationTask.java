package io.evercam.capture.tasks;

import android.location.Location;
import android.os.AsyncTask;

import io.evercam.Camera;
import io.evercam.EvercamException;
import io.evercam.PatchCameraBuilder;

/**
 * Patch camera location
 * <p/>
 * This task will be executed when
 */
public class PatchLocationTask extends AsyncTask<Void, Void, Void>
{
    private Float lng = null;
    private Float lat = null;
    private String cameraId = "";

    public PatchLocationTask(String cameraId, Location location)
    {
        this.cameraId = cameraId;

        if(location != null)
        {
            lng = (float) location.getLongitude();
            lat = (float) location.getLatitude();
        }
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        if(lng != null && lat != null)
        {
            try
            {
                Camera.patch(new PatchCameraBuilder(cameraId).setLocation(lat.toString(), lng
                        .toString()).build());
            }
            catch(EvercamException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }
}
