package io.evercam.capture.tasks;

import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;

import io.evercam.EvercamException;
import io.evercam.PatchCameraBuilder;
import io.evercam.capture.MainActivity;
import io.evercam.capture.R;

/**
 * Patch is_public or camera name.
 */
public class PatchTask extends AsyncTask<Void, Void, Boolean>
{
    private int cameraId;
    private String cameraName;
    private boolean isPublic;
    private String error = "Error";
    private MainActivity mainActivity;

    public PatchTask(int cameraId, String cameraName, boolean isPublic, MainActivity mainActivity)
    {
        this.cameraId = cameraId;
        this.cameraName = cameraName;
        this.isPublic = isPublic;
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPostExecute(Boolean success)
    {
        mainActivity.dismissProgress();
        if(success)
        {
            mainActivity.showShortToast(mainActivity.getString(R.string.camera_updated));
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            {
                mainActivity.updateAfterPatchAPI14();
            }
            else
            {
                mainActivity.updateAfterPatch();
            }
        }
        else
        {
            mainActivity.showShortToast(error);
        }
    }

    @Override
    protected void onPreExecute()
    {
        mainActivity.showProgress(mainActivity.getString(R.string.updating_camera));
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        try
        {
            if(cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
            {
                io.evercam.Camera camera = io.evercam.Camera.patch(new PatchCameraBuilder
                        (mainActivity.frontCamera.getId()).setName(cameraName).setPublic
                        (isPublic).setDiscoverable(isPublic).build());
                mainActivity.frontCamera = camera;
                return true;
            }
            else
            {
                io.evercam.Camera camera = io.evercam.Camera.patch(new PatchCameraBuilder
                        (mainActivity.backCamera.getId()).setName(cameraName).setPublic(isPublic)
                        .setDiscoverable(isPublic).build());
                mainActivity.backCamera = camera;
                return true;
            }
        }
        catch(EvercamException e)
        {
            error = e.getMessage();
            return false;
        }
    }
}