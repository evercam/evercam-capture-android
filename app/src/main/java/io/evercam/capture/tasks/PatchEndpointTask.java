package io.evercam.capture.tasks;

import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.Log;

import io.evercam.EvercamException;
import io.evercam.PatchCameraBuilder;
import io.evercam.capture.MainActivity;

public class PatchEndpointTask extends AsyncTask<Void, Void, Boolean>
{
    private final String TAG = "evercamcapture-PatchEndpointTask";
    private int cameraId;
    private String error = "Error";
    private MainActivity mainActivity;
    private String internalHost;
    private int internalPort;
    private io.evercam.Camera patchedCamera;

    public PatchEndpointTask(int cameraId, MainActivity mainActivity, String internalHost, int
            internalPort)
    {
        this.cameraId = cameraId;
        this.mainActivity = mainActivity;
        this.internalHost = internalHost;
        this.internalPort = internalPort;
    }

    @Override
    protected void onPostExecute(Boolean success)
    {
        if(success)
        {
            mainActivity.appendCameraDetails(patchedCamera);
        }
        else
        {
            mainActivity.showShortToast(error);
        }
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        try
        {
            if(cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
            {
                io.evercam.Camera patchedCamera = io.evercam.Camera.patch(new PatchCameraBuilder
                        (mainActivity.frontCamera.getId()).setInternalHost(internalHost)
                        .setInternalHttpPort(internalPort).build());
                mainActivity.frontCamera = patchedCamera;
                return true;
            }
            else
            {
                io.evercam.Camera patchedCamera = io.evercam.Camera.patch(new PatchCameraBuilder
                        (mainActivity.backCamera.getId()).setInternalHost(internalHost)
                        .setInternalHttpPort(internalPort).build());
                mainActivity.backCamera = patchedCamera;
                return true;
            }
        }
        catch(EvercamException e)
        {
            error = e.getMessage();
            Log.e(TAG, e.toString());
            return false;
        }
    }
}
