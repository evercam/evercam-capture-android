package io.evercam.capture.tasks;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import io.evercam.EvercamException;
import io.evercam.PatchCameraBuilder;
import io.evercam.capture.helper.LocationReader;

/**
 * Patch camera online/off line.
 */
public class PatchOnlineTask extends AsyncTask<Void, Void, Void>
{
    private final String TAG = "PatchOnlineTask";
    private String cameraId;
    private Boolean isOnline;
    private Context context;

    public PatchOnlineTask(String cameraId, Boolean isOnline, Context context)
    {
        this.cameraId = cameraId;
        this.isOnline = isOnline;
        this.context = context;
    }

    @Override
    protected void onPreExecute()
    {
        /*
         * If camera get online, update location data.
		 */
        if(isOnline)
        {
            new LocationReader(context)
            {
                @Override
                public void onLocationUpdated(Location location)
                {
                    new PatchLocationTask(cameraId, location).executeOnExecutor(AsyncTask
                            .THREAD_POOL_EXECUTOR);
                }
            };
        }
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        try
        {
            io.evercam.Camera.patch(new PatchCameraBuilder(cameraId).setOnline(isOnline).build());

        }
        catch(EvercamException e)
        {
            Log.e(TAG, "Error patch is_online" + e.toString());
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
    }

}
