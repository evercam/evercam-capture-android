package io.evercam.capture.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.splunk.mint.Mint;

import java.util.ArrayList;

import io.evercam.Camera;
import io.evercam.capture.MainActivity;
import io.evercam.capture.authentication.EvercamData;
import io.evercam.capture.authentication.EvercamUser;
import io.evercam.capture.helper.NetInfo;

public class DeleteCameraTask extends AsyncTask<Void, Void, Void>
{
    private final String TAG = "DeleteCameraTask";
    private Context ctxt;

    public DeleteCameraTask(MainActivity mainActivity)
    {
        ctxt = mainActivity.getApplicationContext();
    }

    @Override
    protected Void doInBackground(Void... arg0)
    {
        ArrayList<io.evercam.Camera> cameras;
        EvercamUser evercamUser = EvercamData.defaultUser;

        if(evercamUser != null)
        {
            try
            {
                String username = evercamUser.getUsername();
                cameras = Camera.getAll(username, false, false);
                int count = 0;
                for(io.evercam.Camera camera : cameras)
                {
                    if(camera.getMacAddress().equalsIgnoreCase(NetInfo.getMacAddress(ctxt)))
                    {
                        count++;
                    }
                }
                if(count > 2)
                {
                    for(io.evercam.Camera camera : cameras)
                    {
                        if(camera.getMacAddress().equalsIgnoreCase(NetInfo.getMacAddress(ctxt)))
                        {
                            Camera.delete(camera.getId());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Log.e(TAG, e.getMessage());
                Mint.logException(e);
            }
        }
        else
        {
            Log.e(TAG, "Username loaded from DB is null");
        }
        return null;
    }
}
