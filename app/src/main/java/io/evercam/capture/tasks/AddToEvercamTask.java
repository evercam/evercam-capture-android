package io.evercam.capture.tasks;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.splunk.mint.Mint;

import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import io.evercam.Camera;
import io.evercam.CameraBuilder;
import io.evercam.CameraDetail;
import io.evercam.EvercamException;
import io.evercam.capture.MainActivity;
import io.evercam.capture.R;
import io.evercam.capture.authentication.EvercamData;
import io.evercam.capture.helper.CustomedDialog;
import io.evercam.capture.helper.LocationReader;
import io.evercam.capture.helper.NetInfo;
import io.evercam.capture.helper.PrefsManager;

public class AddToEvercamTask extends AsyncTask<Void, Void, String>
{
    private final String TAG = "AddToEvercamTask";
    private MainActivity mainActivity;
    private Context ctxt;
    private String username;
    private String internalHost;
    private int internalPort;
    private String externalHost;
    private int externalPort;
    private String errorCode = "Error";
    private CameraDetail frontDetail;
    private CameraDetail backDetail;
    private int cameraType;
    private final int MAX_ADD_COUNT = 2;

    public AddToEvercamTask(MainActivity mainActivity, String internalHost, int internalPort,
                            String externalHost, int externalPort, int cameraType)
    {
        this.mainActivity = mainActivity;
        this.ctxt = mainActivity.getApplicationContext();

        if(EvercamData.defaultUser != null)
        {
            username = EvercamData.defaultUser.getUsername();
            if(username == null)
            {
                showLoginAgainDialog();
            }
        }
        else
        {
            showLoginAgainDialog();
        }
        this.internalHost = internalHost;
        this.internalPort = internalPort;
        this.externalHost = externalHost;
        this.externalPort = externalPort;
        this.cameraType = cameraType;
    }

    @Override
    protected void onPreExecute()
    {
        mainActivity.appendTextProgress(mainActivity.getString(R.string.log_add_to_evercam));
    }

    @Override
    protected void onPostExecute(final String cameraId)
    {
        if(!cameraId.isEmpty())
        {
            mainActivity.runCameraCheck();

            // If camera is added, patch location
            new LocationReader(mainActivity)
            {
                @Override
                public void onLocationUpdated(Location location)
                {
                    new PatchLocationTask(cameraId, location).executeOnExecutor(AsyncTask
                            .THREAD_POOL_EXECUTOR);
                }
            };
        }
        else
        {
            mainActivity.showShortToast(errorCode);
        }
    }

    @Override
    protected String doInBackground(Void... params)
    {
        if(mainActivity.addTimeCount <= MAX_ADD_COUNT)
        {
            mainActivity.addTimeCount++;
            createDetails();

            Camera createdCamera = null;
            if(cameraType == MainActivity.FRONT_CAMERA)
            {
                createdCamera = createCamera(frontDetail);
            }
            else if(cameraType == MainActivity.BACK_CAMERA)
            {
                createdCamera = createCamera(backDetail);
            }
            try
            {
                if(createdCamera != null)
                {
                    return createdCamera.getId();
                }
            }
            catch(EvercamException e)
            {
                e.printStackTrace();
            }
        }
        return "";
    }

    private void createDetails()
    {
        String mac = NetInfo.getMacAddress(ctxt);

        String frontName = randomNameString(username);
        frontDetail = new CameraBuilder(frontName, false).setMacAddress(mac).setTimeZone
                (TimeZone.getDefault().getID()).setInternalHost(internalHost)
                .setInternalHttpPort(internalPort).setExternalHost(externalHost)
                .setExternalHttpPort(externalPort).setCameraUsername(randomCredientialString
                        ()).setCameraPassword(randomCredientialString()).setJpgUrl
                        (PrefsManager.JPG_URL_FRONT).setVendor(PrefsManager
                        .VENDOR_EVERCAM_CAPTURE).setModel(PrefsManager.MODEL_FRONT).setOnline
                        (true).build();

        String backName = randomNameString(username);
        backDetail = new CameraBuilder(backName, false).setMacAddress(mac).setTimeZone
                (TimeZone.getDefault().getID()).setInternalHost(internalHost)
                .setInternalHttpPort(internalPort).setExternalHost(externalHost)
                .setExternalHttpPort(externalPort).setCameraUsername(randomCredientialString
                        ()).setCameraPassword(randomCredientialString()).setJpgUrl
                        (PrefsManager.JPG_URL_BACK).setVendor(PrefsManager
                        .VENDOR_EVERCAM_CAPTURE).setModel(PrefsManager.MODEL_BACK).setOnline
                        (true).build();
    }

    private Camera createCamera(CameraDetail detail)
    {
        try
        {
            return Camera.create(detail);
        }
        catch(EvercamException e)
        {
            Log.e(TAG, "add camera to evercam: " + e.getMessage());

            sendBugReportWithCameraDetails(e, detail);
            errorCode = e.getMessage();
            return null;
        }
    }

    private String randomCredientialString()
    {
        final String stringSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        int length = 6;

        StringBuilder stringBuilder = new StringBuilder(length);
        for(int i = 0; i < length; i++)
        {
            stringBuilder.append(stringSet.charAt(random.nextInt(stringSet.length())));
        }
        return stringBuilder.toString();
    }

    private String randomNameString(String username)
    {
        final String stringSet = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        int length = 3;

        StringBuilder stringBuilder = new StringBuilder(length);
        for(int i = 0; i < length; i++)
        {
            stringBuilder.append(stringSet.charAt(random.nextInt(stringSet.length())));
        }

        if(username != null && !username.isEmpty())
        {
            String lowercaseUsername = username.toLowerCase(Locale.UK);
            return lowercaseUsername + stringBuilder.toString();
        }
        else
        {
            return "random" + stringBuilder.toString();
        }
    }

    private void sendBugReportWithCameraDetails(EvercamException e, CameraDetail detail)
    {
        HashMap<String, Object> errorDetails = new HashMap<>();
        errorDetails.put("Camera timezone:", detail.getTimezone());
        errorDetails.put("Camera id", detail.getId());
        errorDetails.put("Camera mac address", detail.getMacAddress());
        Mint.logExceptionMap(errorDetails, e);
    }

    private void showLoginAgainDialog()
    {
        try
        {
            CustomedDialog.getLoginAgainDialog(mainActivity, mainActivity.getString(R.string
                    .please_login_again)).show();
            this.cancel(true);
        }
        catch(Exception e)
        {
            // BadTokenException
            Log.e(TAG, e.toString());
        }
    }
}
