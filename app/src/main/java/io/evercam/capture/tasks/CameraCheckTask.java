package io.evercam.capture.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import io.evercam.Camera;
import io.evercam.EvercamException;
import io.evercam.capture.MainActivity;
import io.evercam.capture.authentication.EvercamData;
import io.evercam.capture.helper.NetInfo;

public class CameraCheckTask extends AsyncTask<Void, Void, Void>
{
    private final String TAG = "CameraCheckTask";
    private MainActivity mainActivity;
    private Context ctxt;

    public CameraCheckTask(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
        ctxt = mainActivity.getApplicationContext();
    }

    @Override
    protected void onPostExecute(Void result)
    {
        mainActivity.onCameraCheckDone();
    }

    @Override
    protected Void doInBackground(Void... arg0)
    {
        requestForCameraDetail();
        return null;
    }

    private void requestForCameraDetail()
    {
        ArrayList<io.evercam.Camera> cameras;
        try
        {
            cameras = Camera.getAll(EvercamData.defaultUser.getUsername(), false, false);
            for(io.evercam.Camera camera : cameras)
            {
                if(camera.getMacAddress().equalsIgnoreCase(NetInfo.getMacAddress(ctxt)))
                {
                    if(camera.getInternalJpgUrl().contains("front"))
                    {
                        mainActivity.frontCamera = camera;
                    }
                    else if(camera.getInternalJpgUrl().contains("back"))
                    {
                        mainActivity.backCamera = camera;
                    }
                }
            }
            if(mainActivity.frontCamera != null)
            {
                mainActivity.frontAdded = true;
            }
            if(mainActivity.backCamera != null)
            {
                mainActivity.backAdded = true;
            }
        }
        catch(EvercamException e)
        {
            Log.e(TAG, e.getMessage());
        }
    }
}