package io.evercam.capture.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;
import com.mashape.unirest.http.utils.Base64Coder;
import com.splunk.mint.Mint;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import io.evercam.capture.MainActivity;
import io.evercam.capture.R;
import io.evercam.capture.helper.AssertsMover;
import io.evercam.capture.helper.NetInfo;
import io.evercam.capture.helper.PrefsManager;
import io.evercam.capture.helper.PropertyReader;

public class SshForwardingTask extends AsyncTask<Void, Void, Boolean>
{
    private final static String TAG = "SshForwardingTask";
    private MainActivity mainActivity;
    private int localPort;
    private int remotePort;
    private static Context ctxt;
    private PropertyReader propertyReader;
    private SharedPreferences sharedPrefs;
    private String username;
    private String privateKeyName;
    private String serverIP;
    private final int SSH_PORT = 22;
    private final String LOOP_ADDRESS = "127.0.0.1";

    public SshForwardingTask(MainActivity mainActivity, int localPort)
    {
        this.mainActivity = mainActivity;
        this.localPort = localPort;
        SshForwardingTask.ctxt = mainActivity.getApplicationContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
        propertyReader = new PropertyReader(ctxt);
        username = propertyReader.getPropertyStr(PropertyReader.KEY_SSH_USERNAME);
        privateKeyName = propertyReader.getPropertyStr(PropertyReader.KEY_SSH_PRIVATE_KEY);
        serverIP = propertyReader.getPropertyStr(PropertyReader.KEY_SSH_SERVER);
    }

    @Override
    protected void onPreExecute()
    {
        mainActivity.appendTextProgress(mainActivity.getString(R.string.log_port_forwarding));
    }

    @Override
    protected Boolean doInBackground(Void... arg0)
    {
        remotePort = requestGatewayPort(NetInfo.getMacAddress(ctxt));
        if(remotePort == 0)
        {
            mainActivity.appendTextProgress(mainActivity.getString(R.string
                    .failed_to_request_port));
            return false;
        }
        else
        {
            JSch jsch = new JSch();
            Session session;
            try
            {
                String privateKeyPath = new AssertsMover(ctxt).copyToSdcard(privateKeyName);
                jsch.addIdentity(privateKeyPath);
                session = jsch.getSession(username, serverIP, SSH_PORT);

                UserInfo userInfo = new JschUserInfo();
                session.setUserInfo(userInfo);
                session.connect();
                mainActivity.sshSession = session;

                session.setPortForwardingR(remotePort, LOOP_ADDRESS, localPort);
                return true;
            }
            catch(JSchException e)
            {
                Log.e(TAG, "JschException:" + e.getMessage());
                Mint.logException(e);
            }
            catch(Exception e)
            {
                Mint.logException(e);
                Log.e(TAG, "SSH forwarding: " + e.getMessage());
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean success)
    {
        if(success)
        {
            mainActivity.appendTextProgress(mainActivity.getString(R.string.log_port_forwarded) +
                    "local" + localPort + " > remote" + remotePort);
            PrefsManager.saveURLs(sharedPrefs, NetInfo.getLocalIpAddress(), localPort, serverIP,
                    remotePort);
            mainActivity.runCameraCheck();
        }
        else
        {
            mainActivity.appendTextProgress(mainActivity.getString(R.string.failed_ssh_forwarding));
        }
    }

    private int requestGatewayPort(String mac)
    {
        final String GATEWAY_URL = propertyReader.getPropertyStr(PropertyReader.KEY_SERVLET_URL);
        final String USER_PASS = propertyReader.getPropertyStr(PropertyReader.KEY_SERVLET_AUTH);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(GATEWAY_URL + "?mac=" + mac);
        String encoding = Base64Coder.encodeString(USER_PASS);
        get.setHeader("Authorization", "Basic " + encoding);
        org.apache.http.HttpResponse response = null;
        try
        {
            response = client.execute(get);
            String result = EntityUtils.toString(response.getEntity()).trim();
            if(response.getStatusLine().getStatusCode() == 200)
            {
                return Integer.valueOf(result);
            }
        }
        catch(IOException e)
        {
            Mint.logException(e);
            Log.e(TAG, "RequestGatewayPortIO:" + e.getMessage());
        }
        catch(Exception ex)
        {
            Mint.logException(ex);
            Log.e(TAG, "RequestGatewayPort:" + ex.getMessage());
        }
        return 0;
    }

    public static class JschUserInfo implements UserInfo, UIKeyboardInteractive
    {
        @Override
        public String getPassword()
        {
            return "";
        }

        @Override
        public boolean promptYesNo(String str)
        {
            return true;
        }

        @Override
        public String getPassphrase()
        {
            return null;
        }

        @Override
        public boolean promptPassphrase(String message)
        {
            return true;
        }

        @Override
        public void showMessage(String message)
        {
            Log.v(TAG, message);
        }

        @Override
        public String[] promptKeyboardInteractive(String arg0, String arg1, String arg2, String[]
                arg3, boolean[] arg4)
        {
            return null;
        }

        @Override
        public boolean promptPassword(String arg0)
        {
            return true;
        }
    }
}
