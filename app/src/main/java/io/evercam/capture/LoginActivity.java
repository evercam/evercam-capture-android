package io.evercam.capture;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.splunk.mint.Mint;

import java.util.HashMap;

import io.evercam.API;
import io.evercam.ApiKeyPair;
import io.evercam.EvercamException;
import io.evercam.User;
import io.evercam.capture.authentication.EvercamAccount;
import io.evercam.capture.authentication.EvercamData;
import io.evercam.capture.authentication.EvercamUser;
import io.evercam.capture.helper.Constants;
import io.evercam.capture.helper.CustomedDialog;
import io.evercam.capture.tasks.CheckInternetTask;

public class LoginActivity extends ParentActivity
{
    private final String TAG = "evercam-LoginActivity";
    private UserLoginTask userLoginTask = null;

    private String username;
    private String password;

    private EditText usernameView;
    private EditText passwordView;
    private View loginFormView;
    private View loginStatusView;
    private TextView loginStatusMessageView;
    private TextView signUpLink;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_login);
        setUnderLine();

        usernameView = (EditText) findViewById(R.id.editUsername);
        passwordView = (EditText) findViewById(R.id.editPassword);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent)
            {
                if(id == R.id.login || id == EditorInfo.IME_NULL)
                {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        loginFormView = findViewById(R.id.login_form);
        loginStatusView = findViewById(R.id.login_status);
        loginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                new CheckInternetTaskLogin(LoginActivity.this, CheckInternetTaskLogin.TAG_LOGIN)
                        .execute();
            }
        });

        signUpLink.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                new CheckInternetTaskLogin(LoginActivity.this, CheckInternetTaskLogin.TAG_SIGNUP)
                        .execute();
            }
        });
        hideLogoIfNecessary();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        if(isUserLogged(this))
        {
            setResult(Constants.RESULT_TRUE);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == Constants.REQUEST_CODE_SIGN_UP)
        {
            if(resultCode == Constants.RESULT_TRUE)
            {
                setResult(Constants.RESULT_TRUE);
                finish();
            }
        }
    }

    /**
     * (Currently only for portrait mode)
     * Hide Evercam logo when soft keyboard shows up, and show the logo when keyboard is hidden
     */
    public void adjustLoginFormForKeyboardChange()
    {
        final View activityRootView = findViewById(R.id.login_form);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView
                        .getHeight();
                ImageView logoImageView = (ImageView) findViewById(R.id.icon_imgview);
                //Log.d(TAG, activityRootView.getRootView().getHeight() + " - " +
                // activityRootView.getHeight() + " = " + heightDiff);
                if(heightDiff > activityRootView.getRootView().getHeight() / 3)
                {
                    logoImageView.setVisibility(View.GONE);
                }
                else
                {
                    logoImageView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * Hide logo when landscape, or when soft keyboard showing up in portrait
     */
    public void hideLogoIfNecessary()
    {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            ImageView logoImageView = (ImageView) findViewById(R.id.icon_imgview);
            logoImageView.setVisibility(View.GONE);
        }
        else
        {
            adjustLoginFormForKeyboardChange();
        }
    }

    public void attemptLogin()
    {
        if(userLoginTask != null)
        {
            return;
        }

        usernameView.setError(null);
        passwordView.setError(null);

        username = usernameView.getText().toString();
        password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(password))
        {
            passwordView.setError(getString(R.string.error_password_required));
            focusView = passwordView;
            cancel = true;
        }
        else if(password.contains(" "))
        {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        if(TextUtils.isEmpty(username))
        {
            usernameView.setError(getString(R.string.error_username_required));
            focusView = usernameView;
            cancel = true;
        }
        else if(username.contains(" "))
        {
            usernameView.setError(getString(R.string.error_invalid_username));
            focusView = usernameView;
            cancel = true;
        }

        if(cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            loginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            userLoginTask = new UserLoginTask();
            userLoginTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginStatusView.setVisibility(View.VISIBLE);
            loginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener
                    (new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            loginFormView.setVisibility(View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener
                    (new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
        else
        {
            loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean>
    {
        String error = "Error";

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                ApiKeyPair userKeyPair = API.requestUserKeyPairFromEvercam(username, password);
                String userApiKey = userKeyPair.getApiKey();
                String userApiId = userKeyPair.getApiId();
                API.setUserKeyPair(userApiKey, userApiId);
                User user = new User(username);
                EvercamUser newUser = new EvercamUser(user);
                newUser.setApiKeyPair(userApiKey, userApiId);

                //The new added user is always configured to be default
                new EvercamAccount(LoginActivity.this).add(newUser);
                return true;
            }
            catch(EvercamException e)
            {
                error = e.getMessage();
                Log.e(TAG, error);
                if(!error.contains(getString(R.string.prefix_invalid)) && !error.contains
                        (getString(R.string.prefix_no_user)))
                {
                    sendBugReportWithLoginDetails(e, username, password);
                }
            }
            catch(Exception e)
            {
                CustomedDialog.getAlertDialog(LoginActivity.this, "Error Occured", getString(R
                        .string.fail_to_connect_camera)).show();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success)
        {
            userLoginTask = null;
            showProgress(false);

            if(success)
            {
                setResult(Constants.RESULT_TRUE);
                finish();
            }
            else
            {
                Toast toast = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                passwordView.setText(null);
            }
        }

        @Override
        protected void onCancelled()
        {
            userLoginTask = null;
            showProgress(false);
        }
    }

    public static boolean isUserLogged(Context context)
    {
        EvercamData.defaultUser = new EvercamAccount(context).getDefaultUser();
        if(EvercamData.defaultUser != null)
        {
            API.setUserKeyPair(EvercamData.defaultUser.getApiKey(), EvercamData.defaultUser
                    .getApiId());
            API.setUserKeyPair(EvercamData.defaultUser.getApiKey(), EvercamData.defaultUser
                    .getApiId());
            return true;
        }
        return false;
    }

    private void setUnderLine()
    {
        signUpLink = (TextView) findViewById(R.id.signupLink);
        SpannableString spanString = new SpannableString(this.getResources().getString(R.string
                .create_account));
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        signUpLink.setText(spanString);
    }

    public class CheckInternetTaskLogin extends CheckInternetTask
    {
        public final static int TAG_LOGIN = 0;
        public final static int TAG_SIGNUP = 1;
        private int flag;

        public CheckInternetTaskLogin(Context context, int flag)
        {
            super(context);
            this.flag = flag;
        }

        @Override
        protected void onPostExecute(Boolean hasNetwork)
        {
            if(hasNetwork)
            {
                if(flag == TAG_LOGIN)
                {
                    attemptLogin();
                }
                else if(flag == TAG_SIGNUP)
                {
                    Intent signUpIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                    startActivityForResult(signUpIntent, Constants.REQUEST_CODE_SIGN_UP);
                }
            }
            else
            {
                CustomedDialog.getNoInternetDialog(LoginActivity.this).show();
            }
        }
    }

    private void sendBugReportWithLoginDetails(EvercamException exception, String username,
                                               String password)
    {
        HashMap<String, Object> errorDetails = new HashMap<>();
        errorDetails.put("username", username);
        errorDetails.put("password", password);
        Mint.logExceptionMap(errorDetails, exception);
    }
}
