package io.evercam.capture.server;

import android.content.SharedPreferences;
import android.hardware.Camera;
import android.preference.PreferenceManager;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import io.evercam.capture.MainActivity;
import io.evercam.capture.R;
import io.evercam.capture.helper.PrefsManager;
import io.evercam.capture.helper.TakePictureHelper;

public class CommunicationThread implements Runnable
{
    private Socket clientSocket;
    private MainActivity mainActivity;
    private TakePictureHelper helper;
    private SharedPreferences sharedPrefs;

    public CommunicationThread(Socket clientSocket, MainActivity mainActivity, TakePictureHelper
            helper)
    {
        this.clientSocket = clientSocket;
        this.mainActivity = mainActivity;
        this.helper = helper;
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mainActivity
                .getApplicationContext());
    }

    @Override
    public void run()
    {
        BufferedReader input = null;
        try
        {
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(),
                    "ISO-8859-2"));
            OutputStream output = clientSocket.getOutputStream();

            String request = getStringFromInput(input);
            String[] headers = request.split("\n");

            boolean hasAuth = false;
            String auth = null;
            for(String header : headers)
            {
                if(header.contains("Authorization: Basic") || header.contains("authorization: " +
                        "Basic"))
                {
                    hasAuth = true;
                    auth = header.substring(21, header.length());
                    break;
                }
            }

            if(request.startsWith("GET " + PrefsManager.JPG_URL_FRONT) && mainActivity
                    .selectedCamera != MainActivity.BACK_CAMERA)
            {
                if(hasAuth)
                {
                    String userpass = new String(Base64.decode(auth, 0));
                    if(userpass.equals(PrefsManager.getFrontAuth(sharedPrefs)))
                    {
                        if(mainActivity.selectedCamera == MainActivity.BOTH_CAMERA)
                        {
                            mainActivity.appendTextProgress(mainActivity.getString(R.string
                                    .log_new_front_request));
                        }
                        else
                        {
                            mainActivity.appendTextProgress(mainActivity.getString(R.string
                                    .snapshot_request_received));
                        }

                        mainActivity.updateProgress(mainActivity.frontProgress, true);
                        while(mainActivity.cameraInUse)
                        {
                            Thread.sleep(500);
                            continue;
                        }
                        byte[] data = null;
                        if(mainActivity.selectedCamera == MainActivity.BOTH_CAMERA)
                        {
                            data = helper.takePicture(Camera.CameraInfo.CAMERA_FACING_FRONT);
                        }
                        else if(mainActivity.selectedCamera == MainActivity.FRONT_CAMERA)
                        {
                            data = helper.convertRawFrameToRawImage(helper.currentPreview);
                        }
                        mainActivity.updateProgress(mainActivity.frontProgress, false);
                        sendJPEG(output, data);
                    }
                    else
                    {
                        sendNotAuthorized(output);
                    }
                }
                else
                {
                    sendNotAuthorized(output);
                }
            }
            else if(request.startsWith("GET " + PrefsManager.JPG_URL_BACK) && mainActivity
                    .selectedCamera != MainActivity.FRONT_CAMERA)
            {
                if(hasAuth)
                {
                    String userpass = new String(Base64.decode(auth, 0));
                    if(userpass.equals(PrefsManager.getBackAuth(sharedPrefs)))
                    {
                        if(mainActivity.selectedCamera == MainActivity.BOTH_CAMERA)
                        {
                            mainActivity.appendTextProgress(mainActivity.getString(R.string
                                    .log_new_back_request));
                        }
                        else
                        {
                            mainActivity.appendTextProgress(mainActivity.getString(R.string
                                    .snapshot_request_received));
                        }

                        mainActivity.updateProgress(mainActivity.backProgress, true);
                        while(mainActivity.cameraInUse)
                        {
                            Thread.sleep(500);
                            continue;
                        }
                        byte[] data = null;
                        if(mainActivity.selectedCamera == MainActivity.BOTH_CAMERA)
                        {
                            // keep using take picture API for both camera mode
                            data = helper.takePicture(Camera.CameraInfo.CAMERA_FACING_BACK);
                        }
                        else if(mainActivity.selectedCamera == MainActivity.BACK_CAMERA)
                        {
                            data = helper.convertRawFrameToRawImage(helper.currentPreview);
                        }
                        mainActivity.updateProgress(mainActivity.backProgress, false);
                        sendJPEG(output, data);
                    }
                    else
                    {
                        sendNotAuthorized(output);
                    }
                }
                else
                {
                    sendNotAuthorized(output);
                }
            }
            else
            {
                sendNotFound(output);
            }

            input.close();
            output.flush();
            output.close();
            clientSocket.close();
        }
        catch(Exception ex)
        {
        }
        finally
        {
            try
            {
                input.close();
            }
            catch(IOException ex)
            {
            }
        }
    }

    String getStringFromInput(BufferedReader input)
    {
        StringBuilder sb = new StringBuilder();
        String sTemp;
        try
        {
            while(!(sTemp = input.readLine()).equals(""))
            {
                sb.append(sTemp).append("\n");
            }
        }
        catch(IOException e)
        {
            return "";
        }

        return sb.toString();
    }

    void sendJPEG(OutputStream output, byte[] data)
    {
        try
        {
            String contentType = "image/JPEG";
            String header = "HTTP/1.1 200 OK\n" + "Content-type: " + contentType + "\n" +
                    "Content-Length: " + data.length + "\n" + "\n";
            output.write(header.getBytes());
            output.write(data);
        }
        catch(Exception ex)
        {
        }
    }

    void send(OutputStream output, String s, String header)
    {

        header += "Content-Length: " + s.length() + "\n" + "\n";

        try
        {
            output.write((header + s).getBytes());
        }
        catch(Exception ex)
        {
        }
    }

    private void sendNotFound(OutputStream output)
    {
        String header404 = "HTTP/1.1 404 Not Found\n" + "Connection: close\n" + "Content-type: " +
                "text/html; charset=utf-8\n";
        send(output, "404 Not Found", header404);
    }

    private void sendNotAuthorized(OutputStream output)
    {
        String header401 = "HTTP/1.1 401 Not Authorized\n" + "Connection: close\n" +
                "Content-type: text/html; charset=utf-8\nWWW-Authenticate: Basic " +
                "realm=\"evercam-capture\"\n";
        send(output, "", header401);
    }
}
