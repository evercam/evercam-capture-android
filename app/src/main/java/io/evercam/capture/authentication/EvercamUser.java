package io.evercam.capture.authentication;

import io.evercam.EvercamException;
import io.evercam.User;

public class EvercamUser
{
    private String email = "";
    private String username = "";
    private String firstName = "";
    private String lastName = "";
    private boolean isDefault = false;
    private String apiKey = "";
    private String apiId = "";

    public EvercamUser()
    {

    }

    public EvercamUser(User user) throws EvercamException
    {
        setUsername(user.getUsername());
        setEmail(user.getEmail());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKeyPair(String apiKey, String apiId)
    {
        this.apiKey = apiKey;
        this.apiId = apiId;
    }

    public boolean isDefault()
    {
        return isDefault;
    }

    public void setDefault(boolean isDefault)
    {
        this.isDefault = isDefault;
    }

    public String getApiId()
    {
        return apiId;
    }

    @Override
    public String toString()
    {
        return "EvercamUser{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isDefault=" + isDefault +
                ", apiKey='" + apiKey + '\'' +
                ", apiId='" + apiId + '\'' +
                '}';
    }
}
