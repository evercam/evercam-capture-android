package io.evercam.capture.authentication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.content.Context;

import java.util.ArrayList;

import io.evercam.capture.R;

public class EvercamAccount
{
    private final String TAG = "EvercamAccount";
    private Context mContext;
    private final AccountManager mAccountManager;

    /**
     * Account keys
     */
    public static final String KEY_USERNAME = "username";
    public static final String KEY_API_KEY = "apiKey";
    public static final String KEY_API_ID = "apiId";
    public static final String KEY_IS_DEFAULT = "isDefault";
    public static final String KEY_FIRSTNAME = "firstName";
    public static final String KEY_LASTNAME = "lastName";
    private final String TRUE = "true";

    public EvercamAccount(Context context)
    {
        mContext = context;
        mAccountManager = AccountManager.get(mContext);
    }

    public void add(EvercamUser newUser)
    {
        final Account account = getAccountByEmail(newUser.getEmail());

        mAccountManager.addAccountExplicitly(account, null, null);
        mAccountManager.setAuthToken(account, KEY_API_KEY, newUser.getApiKey());
        mAccountManager.setAuthToken(account, KEY_API_ID, newUser.getApiId());
        mAccountManager.setUserData(account, KEY_USERNAME, newUser.getUsername());
        mAccountManager.setUserData(account, KEY_FIRSTNAME, newUser.getFirstName());
        mAccountManager.setUserData(account, KEY_LASTNAME, newUser.getLastName());

        //Always set the new user as default user
        updateDefaultUser(newUser.getEmail());
    }

    public void remove(final String email, AccountManagerCallback<Boolean> callback)
    {
        final Account account = getAccountByEmail(email);

        String isDefaultString = mAccountManager.getUserData(account, KEY_IS_DEFAULT);
        //If removing default user, clear the static user object
        if(isDefaultString != null && isDefaultString.equals(TRUE))
        {
            EvercamData.defaultUser = null;
        }

        mAccountManager.removeAccount(account, callback, null);
    }


    public Account getAccountByEmail(String email)
    {
        return new Account(email, mContext.getString(R.string.account_type));
    }

    public EvercamUser retrieveUserByEmail(String email)
    {
        Account account = getAccountByEmail(email);
        String apiKey = mAccountManager.peekAuthToken(account, KEY_API_KEY);
        String apiId = mAccountManager.peekAuthToken(account, KEY_API_ID);
        String username = mAccountManager.getUserData(account, KEY_USERNAME);
        String firstName = mAccountManager.getUserData(account, KEY_FIRSTNAME);
        String lastName = mAccountManager.getUserData(account, KEY_LASTNAME);

        String isDefaultString = mAccountManager.getUserData(account, KEY_IS_DEFAULT);

        EvercamUser evercamUser = new EvercamUser();
        evercamUser.setEmail(email);
        evercamUser.setApiKeyPair(apiKey, apiId);
        evercamUser.setUsername(username);
        evercamUser.setFirstName(firstName);
        evercamUser.setLastName(lastName);

        if(isDefaultString != null && isDefaultString.equals(TRUE))
        {
            evercamUser.setDefault(true);
            EvercamData.defaultUser = evercamUser;
        }

        return evercamUser;
    }

    public ArrayList<EvercamUser> retrieveUserList()
    {
        ArrayList<EvercamUser> userList = new ArrayList<>();

        Account[] accounts = mAccountManager.getAccountsByType(mContext.getString(R.string
                .account_type));
        int defaultCount = 0;

        if(accounts.length > 0)
        {
            for(Account account : accounts)
            {
                EvercamUser appUser = retrieveUserByEmail(account.name);
                if(appUser.isDefault())
                {
                    defaultCount++;
                }
                userList.add(appUser);
            }

            //If default user doesn't exist, or more than 1, reset default user
            if(defaultCount != 1)
            {
                EvercamUser newDefaultUser = userList.get(0);
                String defaultUserEmail = newDefaultUser.getEmail();
                updateDefaultUser(defaultUserEmail);
                return retrieveUserList();
            }
        }

        return userList;
    }

    public EvercamUser getDefaultUser()
    {
        ArrayList<EvercamUser> userList = retrieveUserList();

        if(userList.size() > 0)
        {
            for(EvercamUser appUser : userList)
            {
                if(appUser.isDefault())
                {
                    return appUser;
                }
            }
        }
        return null;
    }

    public void updateDefaultUser(String defaultEmail)
    {
        Account[] accounts = mAccountManager.getAccountsByType(mContext.getString(R.string
                .account_type));

        if(accounts.length > 0)
        {
            for(Account account : accounts)
            {
                String email = account.name;
                if(email.equals(defaultEmail))
                {
                    mAccountManager.setUserData(account, KEY_IS_DEFAULT, TRUE);
                    EvercamData.defaultUser = retrieveUserByEmail(email);
                }
                else
                {
                    mAccountManager.setUserData(account, KEY_IS_DEFAULT, "");
                }
            }
        }
    }
}
