package io.evercam.capture;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class AppEntranceActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(LoginActivity.isUserLogged(this))
        {
            startActivity(new Intent(this, MainActivity.class));
        }
        else
        {
            startActivity(new Intent(this, SlideActivity.class));
        }
        finish();
    }
}
