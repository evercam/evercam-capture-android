package io.evercam.capture.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.evercam.Camera;
import io.evercam.EvercamException;
import io.evercam.capture.R;

public class PrefsManager
{
    public final static String KEY_RESOLUTION = "resolution";
    public final static String KEY_FRONT_USERNAME = "frontCameraUsername";
    public final static String KEY_FRONT_PASSWORD = "frontCameraPassword";
    public final static String KEY_BACK_USERNAME = "backCameraUsername";
    public final static String KEY_BACK_PASSWORD = "backCameraPassword";
    public final static String KEY_PORT = "port";
    public final static String KEY_INTERNAL_HOST = "internalHost";
    public final static String KEY_EXTERNAL_HOST = "externalHost";
    public final static String KEY_INTERNAL_PORT = "internalPort";
    public final static String KEY_EXTERNAL_PORT = "externalPort";
    public final static String KEY_SELECTED_CAMERA = "selectedCamera";
    public final static String JPG_URL_FRONT = "/front.jpg";
    public final static String JPG_URL_BACK = "/back.jpg";
    public final static String VENDOR_EVERCAM_CAPTURE = "evercam-capture";
    public final static String MODEL_FRONT = "front-camera-2";
    public final static String MODEL_BACK = "back-camera-2";
    public final static int DEFAULT_PORT = 8080;

    public static void setPort(SharedPreferences sharedPrefs, int port)
    {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(KEY_PORT, port);
        editor.apply();
    }

    public static int getPort(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getInt(KEY_PORT, 0);
    }

    public static void saveCameraCredentials(SharedPreferences sharedPrefs, Camera frontCamera,
                                             Camera backCamera) throws EvercamException
    {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        if(frontCamera != null)
        {
            editor.putString(KEY_FRONT_USERNAME, frontCamera.getUsername());
            editor.putString(KEY_FRONT_PASSWORD, frontCamera.getPassword());
        }
        if(backCamera != null)
        {
            editor.putString(KEY_BACK_USERNAME, backCamera.getUsername());
            editor.putString(KEY_BACK_PASSWORD, backCamera.getPassword());
        }
        editor.apply();
    }

    public static void saveSelectedCamera(SharedPreferences sharedPrefs, int selectedId)
    {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(KEY_SELECTED_CAMERA, selectedId);
        editor.apply();
    }

    public static int getSelectedCamera(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getInt(KEY_SELECTED_CAMERA, -1);
    }

    public static String getFrontAuth(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getString(KEY_FRONT_USERNAME, null) + ":" + sharedPrefs.getString
                (KEY_FRONT_PASSWORD, null);
    }

    public static String getBackAuth(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getString(KEY_BACK_USERNAME, null) + ":" + sharedPrefs.getString
                (KEY_BACK_PASSWORD, null);
    }

    public static void saveURLs(SharedPreferences sharedPrefs, String internalHost, int
            internalPort, String externalHost, int externalPort)
    {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(KEY_INTERNAL_HOST, internalHost);
        editor.putString(KEY_EXTERNAL_HOST, externalHost);
        editor.putInt(KEY_INTERNAL_PORT, internalPort);
        editor.putInt(KEY_EXTERNAL_PORT, externalPort);
        editor.apply();
    }

    public static String getInternalHost(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getString(KEY_INTERNAL_HOST, null);
    }

    public static String getExternalHost(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getString(KEY_EXTERNAL_HOST, null);
    }

    public static int getInternalPort(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getInt(KEY_INTERNAL_PORT, 0);
    }

    public static int getExternalPort(SharedPreferences sharedPrefs)
    {
        return sharedPrefs.getInt(KEY_EXTERNAL_PORT, 0);
    }

    public static String getImageResolution(Context context)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString(KEY_RESOLUTION, context.getString(R.string
                .resolution_vga));
    }

    public static void setImageResolution(String resolution, Context context)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(KEY_RESOLUTION, resolution);
        editor.apply();
    }
}
