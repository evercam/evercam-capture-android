package io.evercam.capture.helper;

public class Constants
{
    public static final int REQUEST_CODE_SIGN_IN = 1;
    public static final int REQUEST_CODE_SIGN_UP = 2;
    public static final int RESULT_TRUE = 1;
}
