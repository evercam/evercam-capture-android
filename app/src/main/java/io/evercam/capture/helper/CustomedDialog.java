package io.evercam.capture.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import io.evercam.EvercamException;
import io.evercam.capture.EvercamCapture;
import io.evercam.capture.MainActivity;
import io.evercam.capture.R;
import io.evercam.capture.tasks.PatchTask;

public class CustomedDialog
{
    // Alert dialog with single button
    public static AlertDialog getAlertDialog(Context context, String title, String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    /**
     * Confirm dialog with yes/no options
     */
    public static AlertDialog getBasicConfirmDialog(Activity activity, int messageId,
                                                    DialogInterface.OnClickListener listener)
    {
        return new AlertDialog.Builder(activity)

                .setMessage(messageId).setPositiveButton(R.string.yes, listener)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                            }
                        }).create();
    }

    public static AlertDialog getLoginAgainDialog(final MainActivity mainActivity, String message)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                mainActivity.finish();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    // Dialog shows when user attempting to leave main page
    public static AlertDialog getLeavePageWarningDialog(final MainActivity mainActivity)
    {
        AlertDialog warningDialog = new AlertDialog.Builder(mainActivity).setTitle(R.string
                .warning).setCancelable(false).setMessage(R.string.msg_leave_page_warning)
                .setPositiveButton(R.string.Continue, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                EvercamCapture.sendEventAnalytics(mainActivity, R.string.category_menu, R.string
                        .action_dashboard, R.string.label_dashboard);
                openDashboardUrlInBrowser(mainActivity);
            }
        }).setNegativeButton(R.string.goback, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        }).create();
        return warningDialog;
    }

    // Dialog to edit Evercam camera name
    public static AlertDialog getEditNameDialog(final MainActivity mainActivity, final int
            hardwareCameraId)
    {
        LayoutInflater mInflater = LayoutInflater.from(mainActivity);
        final View editNameView = mInflater.inflate(R.layout.edit_name_dialog, null);
        final EditText editText = (EditText) editNameView.findViewById(R.id.edit_name_value);
        try
        {
            if(hardwareCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT && mainActivity
                    .frontCamera != null)
            {
                editText.setHint(mainActivity.frontCamera.getName());
            }
            else if(hardwareCameraId == Camera.CameraInfo.CAMERA_FACING_BACK && mainActivity
                    .backCamera != null)
            {
                editText.setHint(mainActivity.backCamera.getName());
            }
        }
        catch(EvercamException e)
        {
            e.printStackTrace();
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
        dialogBuilder.setView(editNameView);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String newName = editText.getText().toString();
                if(newName.equals(""))
                {
                    Toast.makeText(mainActivity, R.string.empty_camera_name, Toast.LENGTH_SHORT)
                            .show();
                }
                else
                {
                    try
                    {
                        EvercamCapture.sendEventAnalytics(mainActivity, R.string.category_edit, R
                                .string.action_edit_name, mainActivity.getString(R.string
                                .label_edit_name) + newName);
                        if(hardwareCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
                        {
                            new PatchTask(hardwareCameraId, newName, mainActivity.frontCamera
                                    .isPublic(), mainActivity).execute();
                        }
                        else if(hardwareCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                        {
                            new PatchTask(hardwareCameraId, newName, mainActivity.backCamera
                                    .isPublic(), mainActivity).execute();
                        }
                    }
                    catch(EvercamException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    // Dialog that shows when Internet is not connected.
    public static AlertDialog getNoInternetDialog(final Context context)
    {
        AlertDialog.Builder connectDialogBuilder = new AlertDialog.Builder(context);
        connectDialogBuilder.setMessage(R.string.noNetworkMsg);

        connectDialogBuilder.setPositiveButton(R.string.settings, new DialogInterface
                .OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        connectDialogBuilder.setNegativeButton(R.string.notNow, new DialogInterface
                .OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        return;
                    }
                });
        connectDialogBuilder.setTitle(R.string.noNetwork);
        connectDialogBuilder.setCancelable(false);
        AlertDialog alertDialog = connectDialogBuilder.create();
        return alertDialog;
    }

    private static void openDashboardUrlInBrowser(Context context)
    {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        PropertyReader propertyReader = new PropertyReader(context);
        String url = propertyReader.getPropertyStr(PropertyReader.KEY_DASHBOARD);
        Uri contentUrl = Uri.parse(url);
        intent.setData(contentUrl);
        context.startActivity(intent);
    }
}
