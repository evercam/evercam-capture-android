package io.evercam.capture.helper;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssertsMover
{
    private Context context;

    public AssertsMover(Context context)
    {
        this.context = context;
    }

    public String copyToSdcard(String fileName)
    {
        final String FOLDER_NAME = "EvercamCapture";
        String absolutePath = "";
        AssetManager assetManager = context.getAssets();

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try
        {
            inputStream = assetManager.open(fileName);
            File folder = new File(Environment.getExternalStorageDirectory() + File.separator +
                    FOLDER_NAME);
            if(!folder.exists())
            {
                folder.mkdirs();
            }
            File outFile = new File(folder.getPath() + File.separator + fileName);
            outputStream = new FileOutputStream(outFile);
            copyFile(inputStream, outputStream);
            inputStream.close();
            inputStream = null;
            outputStream.flush();
            outputStream.close();
            outputStream = null;
            absolutePath = outFile.getAbsolutePath();
        }
        catch(IOException e)
        {
            Log.e("evercamssh", "Failed to copy asset file: " + fileName, e);
        }
        return absolutePath;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }
}
