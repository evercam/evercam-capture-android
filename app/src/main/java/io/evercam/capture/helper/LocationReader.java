package io.evercam.capture.helper;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public abstract class LocationReader
{
    private final String TAG = "capture-LocationReader";
    private LocationManager locationManager;
    private Location currentLocation;

    public LocationReader(Context context)
    {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);

        Log.d(TAG, "Location provider: " + provider);

        if(provider != null)
        {
            launchListener(provider);
        }
        else
        {
            Log.d(TAG, "Location provider is null");
        }
    }

    private void launchListener(String provider)
    {
        LocationListener listener = new LocationListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                if(currentLocation == null)
                {
                    currentLocation = location;
                    onLocationUpdated(location);
                }
                else
                {
                    locationManager.removeUpdates(this);
                }
                Log.d(TAG, location.getLongitude() + "   " + location.getLatitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras)
            {
            }

            @Override
            public void onProviderEnabled(String provider)
            {
            }

            @Override
            public void onProviderDisabled(String provider)
            {
            }
        };

        locationManager.requestLocationUpdates(provider, 0, 0, listener);
    }

    public abstract void onLocationUpdated(Location location);
}
