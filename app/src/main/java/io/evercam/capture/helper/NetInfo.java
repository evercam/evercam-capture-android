package io.evercam.capture.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetInfo
{
    private Context ctxt;

    public NetInfo(Context ctxt)
    {
        this.ctxt = ctxt;
    }

    public boolean hasActiveNetwork()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctxt.getSystemService
                (Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static String getLocalIpAddress()
    {
        try
        {
            for(Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
                    .hasMoreElements(); )
            {
                NetworkInterface intf = en.nextElement();
                for(Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                        .hasMoreElements(); )
                {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if(!inetAddress.isLoopbackAddress())
                    {
                        if(inetAddress instanceof Inet6Address)
                        {
                            continue;
                        }
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        }
        catch(SocketException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMacAddress(Context ctxt)
    {
        WifiManager manager = (WifiManager) ctxt.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        return info.getMacAddress();
    }
}
