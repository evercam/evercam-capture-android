package io.evercam.capture.helper;

import android.content.Context;
import android.content.pm.PackageManager;

public class CameraHelper
{
    public static boolean hasFrontCamera(Context context)
    {
        if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
