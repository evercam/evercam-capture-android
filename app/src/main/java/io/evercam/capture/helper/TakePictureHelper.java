package io.evercam.capture.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.splunk.mint.Mint;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import io.evercam.capture.MainActivity;
import io.evercam.capture.R;

public class TakePictureHelper implements SurfaceHolder.Callback
{
    private final String TAG = "TakePictureHelper";
    public Camera camera;
    private SurfaceView surfaceView;
    public SurfaceHolder surfaceHolder;
    private Camera.PictureCallback pictureCallback;
    private Camera.PreviewCallback previewCallback;
    public byte[] rawData;
    private Object previewLock = new Object();
    private boolean isPreviewRunning = false;
    private final Semaphore ready = new Semaphore(1, true);
    private final Object bufferLock = new Object();
    private int afterPreviewInitDelay = 100;
    private int waitForPic = 3000;
    private MainActivity mainActivity;
    private int currentCameraID;
    public byte[] currentPreview;

    public TakePictureHelper(final MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
        this.surfaceView = mainActivity.surfaceView;
        this.surfaceHolder = surfaceView.getHolder();

        pictureCallback = new Camera.PictureCallback()
        {

            @Override
            public void onPictureTaken(byte[] data, Camera camera)
            {
                synchronized(bufferLock)
                {
                    try
                    {
                        rawData = getRotatedDataFrom(data);
                    }
                    catch(OutOfMemoryError e)
                    {
                        rawData = data;
                    }
                }
            }
        };

        previewCallback = new Camera.PreviewCallback()
        {
            @Override
            public void onPreviewFrame(byte[] frame, Camera camera)
            {
                currentPreview = frame;
            }
        };
        surfaceHolder.addCallback(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        Log.d(TAG, "Surface changed");
        synchronized(previewLock)
        {
            if(isPreviewRunning)
            {
                try
                {
                    camera.stopPreview();
                    isPreviewRunning = false;
                }
                catch(RuntimeException e)
                {
                    // FIXME: Temporary fix for crashing 'method called after
                    // release'
                    Log.e(TAG, "surface changed" + e.getMessage());
                }
            }
        }

        startCameraPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        Log.d(TAG, "Surface created");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        Log.d(TAG, "Surface destroyed");
        synchronized(previewLock)
        {
            if(isPreviewRunning)
            {
                try
                {
                    camera.stopPreview();
                    removePreviewCallback();
                    isPreviewRunning = false;
                }
                catch(RuntimeException e)
                {
                    // FIXME: Temporary fix for crashing 'method called after
                    // release'
                    Log.e(TAG, "surface destroyed" + e.getMessage());
                }
            }
        }

        if(camera != null)
        {
            camera.release();
        }
    }

    public void startCameraPreview()
    {
        try
        {
            if(mainActivity.selectedCamera != MainActivity.FRONT_CAMERA)
            {
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                currentCameraID = Camera.CameraInfo.CAMERA_FACING_BACK;
            }
            else
            {
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                currentCameraID = Camera.CameraInfo.CAMERA_FACING_FRONT;
            }
        }
        catch(Exception e)
        {
            CustomedDialog.getAlertDialog(mainActivity, "Error Occured", mainActivity.getString(R
                    .string.fail_to_connect_camera)).show();
        }

        try
        {
            if(camera != null)
            {
                camera.setPreviewDisplay(surfaceHolder);
                camera = customPreviewSize(camera);
                camera.setDisplayOrientation(90);
                if(mainActivity.selectedCamera != MainActivity.BOTH_CAMERA)
                {
                    addPreviewCallback();
                }
            }
        }
        catch(IOException ex)
        {
            Log.e(TAG, "surface changed" + ex.getMessage());
        }
        catch(Exception e)
        {
            Log.e(TAG, e.toString());
            Mint.logException(e);
        }

        if(camera != null)
        {
            camera.startPreview();
        }
        isPreviewRunning = true;
    }

    public byte[] takePicture(int id)
    {
        if(camera == null)
        {
            return null;
        }
        try
        {
            if(ready.tryAcquire())
            {
                mainActivity.cameraInUse = true;
                if(mainActivity.selectedCamera == MainActivity.BOTH_CAMERA)
                {
                    switchCameraTo(id);
                }
                ensurePreview();
                camera.takePicture(null, null, pictureCallback);
                ready.tryAcquire(waitForPic, TimeUnit.MILLISECONDS);
                ready.release();
                synchronized(previewLock)
                {
                    camera.startPreview();
                    isPreviewRunning = true;
                }
                mainActivity.cameraInUse = false;
                return rawData;
            }
        }
        catch(InterruptedException ex)
        {
            Log.v(TAG, "take picture:" + ex.getMessage());
        }

        return null;
    }

    private void ensurePreview()
    {
        synchronized(previewLock)
        {
            if(!isPreviewRunning)
            {
                camera.startPreview();
                isPreviewRunning = true;
            }

            try
            {
                Thread.sleep(afterPreviewInitDelay);
            }
            catch(InterruptedException ex)
            {
            }
        }
    }

    public void switchCameraTo(int id)
    {
        if(camera != null)
        {
            surfaceDestroyed(surfaceHolder);

            try
            {
                camera.release();
                camera = Camera.open(id);
                currentCameraID = id;
                camera.setPreviewDisplay(surfaceHolder);
                camera = customPreviewSize(camera);
                camera.setDisplayOrientation(90);
                camera.startPreview();
                Thread.sleep(500);
            }
            catch(Exception e)
            {
                Log.e(TAG, "switch camera: " + e.getMessage());
            }
        }
    }

    public byte[] convertRawFrameToRawImage(byte[] frame)
    {
        /* Convert frame to bitmap */
        Parameters parameters = camera.getParameters();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        YuvImage yuvImage = new YuvImage(frame, ImageFormat.NV21, parameters.getPreviewSize()
                .width, parameters.getPreviewSize().height, null);
        yuvImage.compressToJpeg(new Rect(0, 0, parameters.getPreviewSize().width, parameters
                .getPreviewSize().height), 50, out);
        byte[] imageBytes = out.toByteArray();

        try
        {
            return getRotatedDataFrom(imageBytes);
        }
        catch(OutOfMemoryError e)
        {
            return imageBytes;
        }
    }

    private byte[] getRotatedDataFrom(byte[] data) throws OutOfMemoryError
    {
        /* Rotate image for camera view */
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        Matrix matrix = new Matrix();
        if(currentCameraID == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            matrix.setRotate(270);
        }
        else
        {
            matrix.setRotate(90);
        }
        Bitmap result = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, false);
        mainActivity.showPhotoTaken(result, currentCameraID);

		/* Return rotated image */
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        result.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public void removePreviewCallback()
    {
        try
        {
            if(camera != null)
            {
                camera.setPreviewCallback(null);
            }
        }
        catch(Exception e)
        {
            // Exception 'Method called after release()' happen here.
            Log.e(TAG, e.toString());
        }
    }

    public void addPreviewCallback()
    {
        if(camera != null)
        {
            camera.setPreviewCallback(previewCallback);
        }
    }

    public void removeHolderCallback()
    {
        surfaceHolder.removeCallback(this);
    }

    private Camera customPreviewSize(Camera camera)
    {
        Context context = mainActivity.getApplicationContext();
        String resolution = PrefsManager.getImageResolution(context);

        if(resolution.equals(context.getString(R.string.resolution_vga)))
        {
            Camera.Parameters params = camera.getParameters();
            params.setPreviewSize(640, 480);
            camera.setParameters(params);
        }

        return camera;
    }
}
