package io.evercam.capture;

import android.app.Activity;
import android.os.Bundle;

import com.splunk.mint.Mint;

import io.evercam.capture.helper.PropertyReader;

public class ParentActivity extends Activity
{
    private PropertyReader propertyReader;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        propertyReader = new PropertyReader(this);

        initBugSense();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if(propertyReader.isPropertyExist(PropertyReader.KEY_SPLUNK_MINT))
        {
            Mint.startSession(this);
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        if(propertyReader.isPropertyExist(PropertyReader.KEY_SPLUNK_MINT))
        {
            Mint.closeSession(this);
        }
    }

    private void initBugSense()
    {
        if(propertyReader.isPropertyExist(PropertyReader.KEY_SPLUNK_MINT))
        {
            String splunkCode = propertyReader.getPropertyStr(PropertyReader.KEY_SPLUNK_MINT);

            //Disable network monitoring to avoid the server socket crash
            //TODO: Should be enabled when Splunk fixes the bug:
            // http://docs.splunk
            // .com/Documentation/MintAndroidSDK/latest/DevGuide/Disablenetworkmonitoring
            Mint.disableNetworkMonitoring();
            Mint.initAndStartSession(this, splunkCode);
        }
    }
}
