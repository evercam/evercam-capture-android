package io.evercam.captureserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;

public class AllocatePort extends BaseServlet
{
	private static final long serialVersionUID = 1L;
	private final String MAC_REGEX = "^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$";
	private final String USER_PASS = Constants.USER_PASS;
	private final String DB_USERNAME = Constants.DB_USERNAME;
	private final String DB_PASSWORD = Constants.DB_PASSWORD;

	protected void allocatePort(HttpServletRequest request, HttpServletResponse response)
			throws IOException
	{

		Connection connection = null;

		String macAddress = request.getParameter("mac").toUpperCase(Locale.UK);

		if (basicAuthCorrect(request))
		{

			if (macAddress != null && !macAddress.isEmpty() && macAddress.matches(MAC_REGEX))
			{
				try
				{
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(
							"jdbc:mysql://localhost/evercamcapture", DB_USERNAME, DB_PASSWORD);
					Statement statement = connection.createStatement();
					ResultSet resultSet = statement
							.executeQuery("SELECT * FROM  port_mac WHERE mac = '" + macAddress
									+ "'");

					int port = 0;

					// MAC address not exists yet
					if (!resultSet.next())
					{
						ResultSet maxPortResult = statement
								.executeQuery("SELECT MAX(port) FROM port_mac");
						maxPortResult.next();
						int maxPort = maxPortResult.getInt(1);
						port = maxPort + 1;
						statement.execute("INSERT INTO port_mac (port, mac) VALUES ('" + port
								+ "', '" + macAddress + "')");
					}

					// MAC address already exists.
					else
					{
						port = resultSet.getInt(1);
					}

					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
					out.println(port);

				}
				catch (SQLException ex)
				{
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
				}
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				response.setStatus(400);
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("No valid MAC address privided.");
			}
		}
		else
		{
			response.setStatus(401);
			response.setHeader("WWW-Authenticate", "Basic realm=\"" + "Evercam Capture" + "\"");
		}
	}

	public boolean basicAuthCorrect(HttpServletRequest request)
	{
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null)
		{
			StringTokenizer token = new StringTokenizer(authHeader);
			if (token.hasMoreTokens())
			{
				String basic = token.nextToken();

				if (basic.equalsIgnoreCase("Basic"))
				{
					String credentials = new String(Base64.decodeBase64(token.nextToken()));
					if (credentials.equals(USER_PASS))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

}
