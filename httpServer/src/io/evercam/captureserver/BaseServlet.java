package io.evercam.captureserver;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;

public class BaseServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
	}

	public void destroy()
	{
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		allocatePort(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
	}

	/**
	 * Returns a short description of the servlet.
	 **/
	public String getServletInfo()
	{
		return "Evercam Capture Servlet";
	}
	
	protected void allocatePort(HttpServletRequest request, HttpServletResponse response) throws IOException
	{

	}

}
