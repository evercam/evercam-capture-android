#Evercam Capture

Evercam Capture is an Android app that makes your Android devices work as Evercam compatible IP cameras.

## Features

* Automatically add your cameras on Android device to Evercam.
* Automatic SSH port forwarding.
* Random credentials that make your cameras more secure.
* Support both front and back camera.
* Automatically check your network and keep endpoints up-to-date.

## Published App
The app is now unpublished from App Store, if you've already installed it, you might still have access to the link:
<a href="https://play.google.com/store/apps/details?id=io.evercam.capture&hl=en"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/apps/en-play-badge-border.png" width="200"/></a>

## Build

This repo contains both client side (the Android app) and server side code, and the app will only work if the SSH server is set up correctly. 

1. Checkout from Git:  
    ```git clone https://github.com/evercam/evercam-capture-android.git```
2. Setup SSH server following the [steps](https://github.com/evercam/evercam-capture-android/wiki/SSH-Gateway-Server-Setup-(Capture-Server-on-EC2))
3. Open the project in Android Studio and run

## Help make it better

The entire Evercam codebase is open source: http://www.evercam.io/open-source, and we'd love to see your pull requests!

For any bugs and discussions, please use [Github Issues](https://github.com/evercam/evercam-capture-android/issues).

Any questions or suggestions around Evercam, drop us a line: http://www.evercam.io/contact
